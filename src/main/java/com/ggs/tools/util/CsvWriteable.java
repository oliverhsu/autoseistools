package com.ggs.tools.util;

public interface CsvWriteable {

	public String toCsv();
}
