package com.ggs.tools.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateUtil {

	private static final long gpsEpochOffset = 315964800;
	private static final long gpsleapSeconds = 16;
	private static final long nanos = 1000000000l;
	
	/**
	 * Converts Instant to GPS time in microseconds.  Instant takes into account zone information.
	 * @param date
	 * @return
	 */
	public static long getGPSTime(Instant instant){		
			
		instant = instant.minusSeconds(gpsEpochOffset);
		instant = instant.plusSeconds(gpsleapSeconds);
		long gpsMicrosec = (instant.getEpochSecond() * nanos + instant.getNano())/1000;
		return gpsMicrosec;
	}
	
	/**
	 * Converts Date to GPS time in microseconds
	 * @param date
	 * @return
	 */
	public static long getGPSTime(Date date){		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));		
		calendar.setTime(date);		
		Long milliSeconds =  calendar.getTimeInMillis()+gpsleapSeconds*1000;		
		calendar.set(1980, 0, 6, 0, 0, 0);
		long diff = calendar.getTimeInMillis();		
		return (milliSeconds-diff)*1000;
	}
	
	/**
	 * Converts  GPS to Date
	 * @param date
	 * @return
	 */
	public static Date getDateFromGPSTime(long gpsTime){
		//Convert Milliseconds
		gpsTime = gpsTime/1000;		
		GregorianCalendar gregorianCalendar = new GregorianCalendar(1980,Calendar.JANUARY,6,0,0,0);		
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));	
		long diff = gregorianCalendar.getTimeInMillis();
		return new Date(diff+gpsTime-gpsleapSeconds*1000);			
	}
	
	/**
	 * Converts date to epoch time
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static long getEpochTime(Date date){
		return date.getTime() / 1000;
	}
	
	/**
	 * Converts epoch to date
	 * @param epochTime
	 * @return
	 */
	public static Date getDateFromEpochTime(long epochTime){
		return new Date(epochTime*1000);
	}
	
	/**
	 * Converts date to testif-I format
	 * @param date
	 * @return
	 */
	public static String getTestifIFromDate(Date date){
		String testIfI = null;
		SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
		testIfI = "001   000000 us U + " + TIME_FORMAT.format(date)  + " 01 01 " + DATE_FORMAT.format(date);		
		return testIfI;
	}
	
	/**
	 * Converts testif-I format to date
	 * @param testIfI
	 * @return
	 * @throws ParseException
	 */
	public static Date getDateFromTestifIFormat(String testIfI) throws ParseException{
		Date date = null;
		if(testIfI!=null && !testIfI.isEmpty()){			
			testIfI = testIfI.trim().replaceAll("( )+", "$1");
			String[] strArray = testIfI.split(" ");
			String strDate =null;
			String strTime =null;
			if(strArray.length>=9){
				strDate = strArray[8];
				strTime = strArray[5];
			}
			if(strDate!=null && strTime!=null){
				SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String strDateTime = strDate.trim() + " " + strTime.trim();				
				date = DATE_TIME_FORMAT.parse(strDateTime);				
			}else if(strDate!=null){
				SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
				date = DATE_FORMAT.parse(strDate);
			}			
		}
		return date;
	}
	
	/**
	 * Convert LocalDateTime to date
	 * @param dateTime
	 * @return
	 */
	public static Date getDate(LocalDateTime dateTime, ZoneId zoneId){
		Instant instant = dateTime.atZone(zoneId).toInstant();		
		return Date.from(instant);
	}

}
