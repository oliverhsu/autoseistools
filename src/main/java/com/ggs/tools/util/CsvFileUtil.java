package com.ggs.tools.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


public class CsvFileUtil {
	
	/**
	 * Writes List<CsvWriteable> bean to specified file
	 * @param fileName csv filename to write
	 * @param dataList List<? extends CsvWriteable> of data to write
	 * @throws IOException
	 */
	public static void writeCsvFile(Path filePath,  List<? extends CsvWriteable> dataList) throws IOException {
		
		try (BufferedWriter csvWriter = Files.newBufferedWriter(filePath)) {
			
			if (!dataList.isEmpty()) {
				
				// write the data
				for (CsvWriteable data : dataList) {
					csvWriter.write(data.toCsv());
					csvWriter.newLine();
				}

			}
			
		}
		
	}
	

}
