package com.ggs.tools.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.FileChooser.ExtensionFilter;

public class FileDialogs {

	public static File saveFileChooser(String title, File initialDirectory, Window window,
			List<ExtensionFilter> extensions, String initialFileName) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(title);
		fileChooser.setInitialDirectory(initialDirectory);
		if (initialFileName != null) {
			fileChooser.setInitialFileName(initialFileName);
		}
		if (extensions != null) {
			fileChooser.getExtensionFilters().addAll(extensions);
		}
		File outputFile = null;
		outputFile = fileChooser.showSaveDialog(window);
		return outputFile;
	}

	public static File showFileChooser(String title, File initialDirectory, Window window) {
		List<File> results = showFileChooser(title, initialDirectory, window, null, false);
		if (!results.isEmpty()) {
			return results.get(0);
		} else {
			return null;
		}
	}

	public static List<File> showFileChooser(String title, File initialDirectory, Window window,
			List<ExtensionFilter> extensions, boolean selectMultipleFiles) {
		List<File> results = new ArrayList<File>();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(title);
		fileChooser.setInitialDirectory(initialDirectory);
		if (extensions != null) {
			fileChooser.getExtensionFilters().addAll(extensions);
		}

		if (selectMultipleFiles) {
			results = fileChooser.showOpenMultipleDialog(window);
		} else {
			results.add(fileChooser.showOpenDialog(window));
		}

		return results;
	}

	public static File showDirChooser(String title, File initialDirectory, Window window) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle(title);
		directoryChooser.setInitialDirectory(initialDirectory);
		return directoryChooser.showDialog(window);
	}

}
