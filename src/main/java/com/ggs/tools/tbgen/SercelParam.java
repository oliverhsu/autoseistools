//
//	module	      :	SercelParam.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the SercelParam class for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	14feb2013	jeff thomson	extracted from TBGenMainWindow.java.
//	05mar2013	jeff thomson	remove Init() method.
//      14mar2013       jeff thomson    use file_number instead of shot_number.
//

package com.ggs.tools.tbgen;


public class SercelParam
{
	public SercelParam()
	{
	    file_number	    = 0;
	    line	    = 0.0f;
	    station	    = 0.0f;
	    point_index	    = 0;
	    gps_time	    = 0;
	    acq_length	    = 0;
	    record_length   = 0;
	    total_samples   = 0;
	    uphole_time	    = 0;
	    source_type	    = new char[] { '\0', '\0', '\0' };
	    file_number	    = 0;
	    tape_number	    = 0;
	    cog_easting	    = 0.0;
	    cog_northing    = 0.0;
	    cog_elevation   = 0.0;
	    cog_deviation   = 0.0;
	    hour	    = 0;
	    minute	    = 0;
	    second	    = 0;
	    shot_time	    = new char[] { '\0', '\0', '\0', '\0',
					   '\0', '\0', '\0', '\0',
					   '\0', '\0', '\0' };
	    julian_day	    = 0;
	    hdr		    = new HeaderParam();
	}

	public int file_number;
	public float line;
	public float station;
	public short point_index;
	public long gps_time;
	public int acq_length;
	public int record_length;
	public int total_samples;
	public short uphole_time;
	public char[] source_type;
	public int tape_number;
	public double cog_easting;
	public double cog_northing;
	public double cog_elevation;
	public double cog_deviation;
	public short hour;
	public short minute;
	public short second;
	public char[] shot_time;
	public int julian_day;
	public HeaderParam hdr;
}

//
//	end of SercelParam.java.
//
