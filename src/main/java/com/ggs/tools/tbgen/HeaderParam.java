//
//	module	      :	HeaderParam.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the HeaderParam class for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	04mar2013	jeff thomson	extracted from SerccelParam.java.
//	12mar2013	jeff thomson	change line/station to sps_line/
//					sps_station;
//					add src_line/src_station.
//

package com.ggs.tools.tbgen;


public class HeaderParam
{
	public HeaderParam()
	{
	    sps_line	    = 0.0f;
	    sps_station	    = 0.0f;
	    src_line	    = 0.0f;
	    src_station	    = 0.0f;
	    point_index	    = 0;
	    source_type	    = new char[] { '\0', '\0', '\0' };
	    uphole_time	    = 0;
	    cog_easting	    = 0.0;
	    cog_northing    = 0.0;
	    cog_elevation   = 0.0;
	    julian_day	    = 0;
	    shot_time	    = new char[] { '\0', '\0', '\0', '\0',
					   '\0', '\0', '\0', '\0',
					   '\0', '\0', '\0' };

	    hour	    = 0;
	    minute	    = 0;
	    second	    = 0;
	    gps_time	    = 0;
	}

	public float sps_line;
	public float sps_station;
	public float src_line;
	public float src_station;
	public short point_index;
	public char[] source_type;
	public short uphole_time;
	public double cog_easting;
	public double cog_northing;
	public double cog_elevation;
	public int julian_day;
	public char[] shot_time;
	public short hour;
	public short minute;
	public short second;
	public long gps_time;
}

//
//	end of HeaderParam.java.
//
