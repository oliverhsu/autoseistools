//
//	module	      :	MyTextArea.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements MyTextArea class for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	25feb2013	jeff thomson	new code.
//	26feb2013	jeff thomson	add date/time stamp to messages.
//	26feb2013	jeff thomson	fix error message.
//      14mar2013       jeff thomson    add setter for log file to ensure that
//                                      each run's log file is unique.
//	22apr2013	jeff thomson	ensure unique log file names.
//	22apr2013	jeff thomson	add support for new log file naming
//					convention.
//	22apr2013	jeff thomson	add support for new log file naming
//	29apr2013	jeff thomson	clear message window between runs.
//	29apr2013	jeff thomson	fix message clearing bug.
//

package com.ggs.tools.tbgen;

import java.io.*;
import java.text.*;
import java.util.*;

public class MyTextArea extends javax.swing.JTextArea
{
	//
	//      class variables.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 447743287457836231L;
	
	private String		    strLogDir	= "";

	public void setstrLogDir( String st )
	{
	    if ( fos != null )
	    {
		try
		{
		    fos.close();
		}
		catch ( IOException e )
		{
		    System.err.println( "Error closing file: " + e );
		}

		fos = null;
	    }

	    strLogDir	= st;

	    this.setText( "" );
	}

	public String getstrLogDir()
	{
	    return strLogDir;
	}

	private String		    strProjName	= "";

	public void setstrProjName( String st )
	{
	    if ( fos != null )
	    {
		try
		{
		    fos.close();
		}
		catch ( IOException e )
		{
		    System.err.println( "Error closing file: " + e );
		}

		fos = null;
	    }

	    strProjName	= st;

	    this.setText( "" );
	}

	public String getstrProjName()
	{
	    return strProjName;
	}

	private String		    strFileName	= "";

	public void setstrFileName( String st )
	{
	    if ( fos != null )
	    {
		try
		{
		    fos.close();
		}
		catch ( IOException e )
		{
		    System.err.println( "Error closing file: " + e );
		}

		fos = null;
	    }

	    strFileName	= st;
	}

	public String getstrFileName()
	{
	    return strFileName;
	}

	private String		    strFileType	= "";

	public void setstrFileType( String st )
	{
	    if ( fos != null )
	    {
		try
		{
		    fos.close();
		}
		catch ( IOException e )
		{
		    System.err.println( "Error closing file: " + e );
		}

		fos = null;
	    }

	    strFileType	= st;
	}

	public String getstrFileType()
	{
	    return strFileType;
	}

	private FileOutputStream    fos	= null;

	//
	//      constructor.
	//

	public MyTextArea()
	{
	}

	//
	//      call base class's append() method before writing
	//	message to disk.
	//

	@Override
	public void append( String str )
	{
	    SimpleDateFormat formatter	= new SimpleDateFormat( "ddMMMyyyy HH:mm:ss" );
	    Date date			= new Date();

	    String	strLogName;
	    String[]	tokens;

	    //
	    //	    call the base class's method.
	    //

	    str	= formatter.format( date ) + " : " + str;

	    super.append( str );

	    //
	    //	    move caret to end of text in window.
	    //

	    try
	    {
		super.setCaretPosition( super.getCaretPosition() + str.length() );
	    }
	    catch ( IllegalArgumentException e )
	    {
	    }

	    //
	    //	    create the output file, if it doesn't already exist.
	    //

	    if ( fos == null )
	    {
		tokens	    = strFileName.split( "\\.(?=[^\\.]+$)" );
		formatter   = new SimpleDateFormat( "MMddyy-HHmmss" );
		strLogName  = strLogDir +
			      strProjName + "_" +
			      tokens[ 0 ] + "-" +
			      strFileType + "_" +
			      formatter.format( date ) + ".log";

		try
		{
		    fos   = new FileOutputStream( strLogName );
		}
		catch ( IOException e )
		{
		    System.err.println( "Error opening output file: " + e );
		}
	    }

	    //
	    //	    write the text to the output file.
	    //

	    try
	    {
		fos.write( str.getBytes() );
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error writing to output file: " + e );
	    }
	}

	//
	//      destructor.
	//

	@Override
	protected void finalize() throws Throwable
	{
	    //
	    //	    close the output file.
	    //

	    try
	    {
		fos.close();
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error closing file: " + e );
	    }

	    //
	    //	    call the base class's method.
	    //

	    super.finalize();
	} 
}

//
//	end of MyTextArea.java.
//
