//
//	module	      :	TBGen.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the entry point for the TBGen
//			application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	15jan2013	jeff thomson	new code.
//	12apr2013	jeff thomson	move window closing event handler to
//					main window class.
//

package com.ggs.tools.tbgen;


public class TBGen
{
	//
	//	@param args the command line arguments
	//

	public static void main( String args[] )
	{
	    //
	    //	    Set the Nimbus look and feel
	    //

	    //
	    //	    <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
	    //	    If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
	    //	    For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
	    //

	    try
	    {
		for ( javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels() )
		{
		    if ( "Nimbus".equals( info.getName() ) )
		    {
			javax.swing.UIManager.setLookAndFeel( info.getClassName() );
			break;
		    }
		}
	    }
	    catch ( ClassNotFoundException ex )
	    {
		java.util.logging.Logger.getLogger( TBGenMainWindow.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
	    }
	    catch ( InstantiationException ex )
	    {
		java.util.logging.Logger.getLogger( TBGenMainWindow.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
	    }
	    catch ( IllegalAccessException ex )
	    {
		java.util.logging.Logger.getLogger( TBGenMainWindow.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
	    }
	    catch ( javax.swing.UnsupportedLookAndFeelException ex )
	    {
		java.util.logging.Logger.getLogger( TBGenMainWindow.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
	    }
	    //</editor-fold>

	    //
	    //	    Create and display the dialog
	    //

	    java.awt.EventQueue.invokeLater(	new Runnable()
						{
						    public void run()
						    {
							TBGenMainWindow dialog = new TBGenMainWindow( new javax.swing.JFrame(), true );

							dialog.setVisible( true );
						    }
						} );
	}
}

//
//	end of TBGen.java.
//
