//
//	module	      :	ProgressData.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the ProgressData class for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	14feb2013	jeff thomson	extracted from TBGenMainWindow.java.
//

package com.ggs.tools.tbgen;


public class ProgressData
{
	public int processed;
	public int read;
}

//
//	end of ProgressData.java.
//
