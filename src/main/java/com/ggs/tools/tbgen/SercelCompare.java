//
//	module	      :	SercelCompare.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the SercelCompare class for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	19feb2013	jeff thomson	new code.
//

package com.ggs.tools.tbgen;

public class SercelCompare implements java.util.Comparator
{
	@Override public int compare( Object o1, Object o2 )
	{
	    SercelParam	ia = ( SercelParam ) o1;
	    SercelParam	ib = ( SercelParam ) o2;

	    int	    len_diff = ia.acq_length - ib.acq_length;
	    long    gps_diff = ia.gps_time   - ib.gps_time;

	    if ( len_diff > 0 )
	    {
		return 1;
	    }
	    else if ( len_diff < 0 )
	    {
		return -1;
	    }
	    else
	    {
		if ( gps_diff > 0 )
		{
		    return 1;
		}
		else if ( gps_diff < 0 )
		{
		    return -1;
		}
		else
		{
		    return 0;
		}
	    }
	}
}

//
//	end of SercelCompare.java.
//
