//
//	module	      :	FileReader.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the FileReader class for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	14feb2013	jeff thomson	extracted from TBGenMainWindow.java.
//	19feb2013	jeff thomson	eliminate duplicate lines;
//					sort data before writing CSV file.
//	22feb2013	jeff thomson	rewrite to support batch
//					processing of raw files;
//					display file name in message
//					window before processing;
//					for now, only output first 7
//					fields.
//	26feb2013	jeff thomson	add support for logging data
//					formatting/decoding errors.
//	05mar2013	jeff thomson	add consistency checks and
//					revised output format.
//	11mar2013	jeff thomson	strip off swath appended to line
//					name in SEGD user header.
//	14mar2013	jeff thomson	more logging and output fine
//					tuning.
//	21mar2013	jeff thomson	fix GPS time decoding bug.
//	22mar2013	jeff thomson	fix SEGD file number decoding bug.
//	28mar2013	jeff thomson	revert to using SPS header shot time.
//	02apr2013	jeff thomson	restore Sercel 428 functionality.
//	10apr2013	jeff thomson	debug Sercel 428 processing.
//	10apr2013	jeff thomson	change "Record Length" to "Acquisition
//					Length".
//	16apr2013	jeff thomson	add support for GPS time adjustment.
//	19apr2013	jeff thomson	add support for per-source type GPS time
//					adjustment.
//	22apr2013	jeff thomson	add support for new csv file naming
//					convention;
//					write program settings to log file.
//	23apr2013	jeff thomson	correct source type enconding for
//					dynamite when processing RAW files;
//					write state of time adjustment flag to
//					log file.
//	29apr2013	jeff thomson	make sure RAW file version is consistent
//					with selected data type.
//	29apr2013	jeff thomson	fix file version decoding bug.
//	02may2013	jeff thomson	sort SEGD output by GPS time.
//	02may2013	jeff thomson	sort SEGD filenames before processing.
//	06may2013	jeff thomson	output SEGD uphole time in milliseconds
//					instead of microseconds.
//	27sep2013	jeff thomson	add support for output of first seven
//					fields only;
//					set source type to 'A1' if BigShot or
//					LongShot detected in user header.
//	27sep2013	jeff thomson	fix air gun detection.
//	18oct2013	jeff thomson	fix air gun detection (again).
//	31oct2013	jeff thomson	add source type to 'seven only' output;
//					extract julian day from GHB 1 for
//					428 SEGD.
//	04nov2013	jeff thomson	add uphole time to 'short form' 428
//					output;
//					fix formatting issues.
//	16dec2013	jeff thomson	fix bug in ProcessSEGDFile() where
//					station number was not being decoded
//					correctly, causing problems with
//					half-stations.
//	03mar2014	jeff thomson	fix SEGD 428 record length bug.
//	04mar2014	jeff thomson	update number of samples calculation.
//	10mar2014	jeff thomson	more number of samples tweaking.
//	24apr2014	jeff thomson	fix CSV file formatting.
//      30sep2014       jeff thomson    fix bug processing 428 RAW files caused
//                                      by null sp value in ProcessRawFile().
//      14oct2014       jeff thomson    fix bug in SEGD uphole time conversion.
//

package com.ggs.tools.tbgen;

import java.io.*;
import java.text.SimpleDateFormat;

import java.util.*;
import java.util.regex.*;
import javax.swing.*;

import com.ggs.tools.tbgen.ProgressData;
import com.ggs.tools.tbgen.TBGenMainWindow.InputType;
import com.ggs.tools.tbgen.TBGenMainWindow.SercelType;

public class FileReader extends SwingWorker< StringBuilder, ProgressData >
{
	//
	//	Class constants.
	//

	private static final int EOF	= -1;

	//
	//	constructor.
	//

	public FileReader( File[] files,
			    SercelType sercel,
			    InputType input,
			    JProgressBar progressbar,
			    MyTextArea textarea,
			    boolean bAdjustTime,
			    TBGenSettings settings )
	{
	    this.files		= files;
	    this.sercel		= sercel;
	    this.input		= input;
	    this.progressbar	= progressbar;
	    this.textarea	= textarea;
	    this.bAdjustTime	= bAdjustTime;
	    this.settings	= settings;
	    this.processed	= 0;
	}

	//
	//	the following method executes in the worker thread;
	//	it doesn't touch Swing components
	//

	@Override
	public StringBuilder doInBackground() throws IOException, InterruptedException
	{
	    SimpleDateFormat formatter		= new SimpleDateFormat( "MMddyy-HHmmss" );
	    List< File > fList			= new ArrayList< File >();
	    List< SercelParam > spList		= new ArrayList< SercelParam >();
	    Comparator< File > byFileName	= new FileNameComparator();
	    Comparator< SercelParam > byGPSTime	= new GPSTimeComparator();
	    Date date				= new Date();

	    String	strTemp;
	    String[]	tokens;

	    //
	    //	    open the input and output files.
	    //

	    switch ( input )
	    {
		case INPUT_SEGD :

		    tokens     = files[ 0 ].getName().split( "\\.(?=[^\\.]+$)" );
		    strOutput  = textarea.getstrLogDir() +
				 textarea.getstrProjName() + "_" +
				 tokens[ 0 ] + "-" +
				 "segd" + "_" +
				 formatter.format( date ) + ".csv";

		    textarea.setstrFileName( files[ 0 ].getName() );
		    textarea.setstrFileType( "segd" );

		    textarea.append( "Project Name: " + settings.strProjectName + "\n" );
		    textarea.append( String.format( "Time Adjustment: %s\n", bAdjustTime ? "Enabled" : "Disabled" ) );
		    textarea.append( String.format( "Dynamite Offset: %7.3f\n", settings.fDynamiteOffset ) );
		    textarea.append( String.format( "Vibrator Offset: %7.3f\n", settings.fVibratorOffset ) );
		    textarea.append( String.format( "Air Gun Offset: %7.3f\n", settings.fAirGunOffset ) );
		    textarea.append( "Processing directory: " + textarea.getstrLogDir() + "\n" );
		    textarea.append( "Output file: " + strOutput + "\n" );

		    try
		    {
			fos   = new FileOutputStream( strOutput );
		    }
		    catch ( IOException e )
		    {
			System.err.println( "Error opening output file: " + e );
		    }

		    if ( sercel == SercelType.SERCEL_408 )
		    {
			if ( this.settings.bFirstNine )
			{
			    fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, Uphole Time, Source Type\n" ).getBytes() );
			}
			else
			    fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, Uphole Time, Source Type\n" ).getBytes() );
		    }
		    else
		    {
			if ( this.settings.bFirstNine )
			{
			    fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, Uphole Time, Source Type\n" ).getBytes() );
			}
			else
			    fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, Uphole Time, Source Type, Easting, Northing, Elevation, Deviation, Tape Number, Total Samples\n" ).getBytes() );
		    }

		    //
		    //	    sort the filenames before processing.
		    //

		    for ( File file : files )
		    {
			fList.add( file );
		    }

		    Collections.sort( fList, byFileName );

		    for ( File file : fList )
		    {
			strTemp	= "Processing file " + file.getName() + "\n";
		    
			textarea.append( strTemp );
		    
			spList.add( ProcessSEGDFile( file, fos ) );
		    
			//
			//	update the progress indicator.
			//
		    
			processed++;
		    
			data		= new ProgressData();
			data.processed  = processed;
		    
			publish( data );
		    }

		    //
		    //	    write the output.
		    //

		    Collections.sort( spList, byGPSTime );

		    for ( SercelParam spo : spList )
		    {
			WriteCSVFile( spo, fos, settings.bFirstNine );
		    }

		    textarea.append( processed + " files processed.\n" );
		    break;

		case INPUT_RAW  :

		    for ( File file : files )
		    {
			tokens     = file.getName().split( "\\.(?=[^\\.]+$)" );

			strOutput  = textarea.getstrLogDir() +
				     textarea.getstrProjName() + "_" +
				     tokens[ 0 ] + "-" +
				     "raw" + "_" +
				     formatter.format( date ) + ".csv";

			textarea.setstrFileName( file.getName() );
			textarea.setstrFileType( "raw" );

			textarea.append( "Project Name: " + settings.strProjectName + "\n" );
			textarea.append( String.format( "Time Adjustment: %s\n", bAdjustTime ? "Enabled" : "Disabled" ) );
			textarea.append( String.format( "Dynamite Offset: %7.3f\n", settings.fDynamiteOffset ) );
			textarea.append( String.format( "Vibrator Offset: %7.3f\n", settings.fVibratorOffset ) );
			textarea.append( String.format( "Air Gun Offset: %7.3f\n", settings.fAirGunOffset ) );

			strTemp	= "Processing file " + file.getPath() + "\n";

			textarea.append( strTemp );
			textarea.append( "Output file: " + strOutput + "\n" );

			try
			{
			    fos   = new FileOutputStream( strOutput );
			}
			catch ( IOException e )
			{
			    System.err.println( "Error opening output file: " + e );
			}

			if ( sercel == SercelType.SERCEL_408 )
			{
			    if ( this.settings.bFirstNine )
			    {
				fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, Uphole Time, Source Type\n" ).getBytes() );
			    }
			    else
				fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, # Line, Station, Source Type\n" ).getBytes() );
			}
			else
			{
			    if ( this.settings.bFirstNine )
			    {
				fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, Uphole Time, Source Type\n" ).getBytes() );
			    }
			    else
				fos.write( ( "# Line, Station, Shot Time, GPS Time, File Number, Acquisition Length, Point Index, Uphole Time, Source Type, Easting, Northing, Elevation, Deviation, Tape Number, Total Samples\n" ).getBytes() );
			}

			if ( !ProcessRawFile( file, fos ) )
			{
			    break;
			}
		    }

		    textarea.append( processed + " records processed.\n" );
		    break;

		case INPUT_NAV  :

		    ProcessNavFile( files[ 0 ], fos );
		    break;

		default	    :

		    break;
	    }

	    return new StringBuilder();
	}

	//
	//	the following methods execute in the event dispatch
	//	thread
	//

	@Override
	public void process( List< ProgressData > data )
	{
	    if ( isCancelled() )
	    {
		return;
	    }

	    for ( ProgressData d : data )
	    {
		progressbar.setValue( d.processed );
	    }
	}

	@Override
	public void done()
	{
	    try
	    {
		fos.close();
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error closing output file: " + e );
	    }

	    textarea.append( "Done." + "\n" );
	}

	//
	//	generate CSV file output from SEG-D file.
	//

	private SercelParam ProcessSEGDFile( File file, FileOutputStream fos )
	{
	    SercelParam sp	= new SercelParam();
	    byte[]	usrhdr	= new byte[ 1025 ];
	    char[]	filenum	= new char[] { '\0', '\0', '\0', '\0',
					       '\0', '\0', '\0', '\0' };

	    String  strHdr;
	    int	    nSampleRate;

	    try
	    {
		fis = new FileInputStream( file.getPath() );
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error opening input file: " + e );
	    }

	    byte[] line	= new byte[ 1494 ];

	    int	content;

	    //
	    //	    read the SEGD data.
	    //

	    try
	    {
		if ( ( content = fis.read( line, 0, 1494 ) ) != -1 )
		{
		    //
		    //	    FFID
		    //

		    short   sVal;
		    int	    iVal;
		    long    lVal;
                    double  dVal;
		    String  strVal;

		    sVal	= ( short ) ( ( ByteToInt( line[ 0x00 ] ) << 8 ) |
						ByteToInt( line[ 0x01 ] ) );

		    if ( sVal == -1 )
		    {
			sp.file_number =    ( ByteToInt( line[ 0x20 ] ) << 16 ) |
					    ( ByteToInt( line[ 0x21 ] ) <<  8 ) |
					      ByteToInt( line[ 0x22 ] );
		    }
		    else
		    {
			filenum[ 0 ]	= HexDigit( ( ( int ) line[ 0x00 ] & 0xF0 ) >> 4 );
			filenum[ 1 ]	= HexDigit(   ( int ) line[ 0x00 ] & 0x0F	 );
			filenum[ 2 ]	= HexDigit( ( ( int ) line[ 0x01 ] & 0xF0 ) >> 4 );
			filenum[ 3 ]	= HexDigit(   ( int ) line[ 0x01 ] & 0x0F	 );

			strVal	= ( new String( filenum ) ).trim();

			try
			{
			    sp.file_number  = Integer.parseInt( strVal );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding file number: " + e + "\n" );

			    sp.file_number  = 0;
			}
		    }

		    //
		    //	    Line
		    //

		    sp.line = ( float ) ( ( ByteToInt( line[ 0x43 ] ) << 16 ) |
					  ( ByteToInt( line[ 0x44 ] ) <<  8 ) |
					    ByteToInt( line[ 0x45 ] ) ) +
			      ( float ) ( ( ByteToInt( line[ 0x46 ] ) <<  8 ) |
					    ByteToInt( line[ 0x47 ] ) ) / 65536.0f;

		    //
		    //	    Station
		    //

		    sp.station = ( float ) ( ( ByteToInt( line[ 0x48 ] ) << 16 ) |
					     ( ByteToInt( line[ 0x49 ] ) <<  8 ) |
					       ByteToInt( line[ 0x4A ] ) ) +
				 ( float ) ( ( ByteToInt( line[ 0x4B ] ) <<  8 ) |
					       ByteToInt( line[ 0x4C ] ) ) / 65536.0f;

		    //
		    //	    Point Index
		    //

		    sp.point_index = ( short ) ByteToInt( line[ 0x4D ] );

		    //
		    //	    Time
		    //

		    sp.shot_time[ 0 ]   = HexDigit( ( ( int ) line[ 0x0D ] & 0xF0 ) >> 4 );
		    sp.shot_time[ 1 ]   = HexDigit(   ( int ) line[ 0x0D ] & 0x0F	 );
		    sp.shot_time[ 2 ]   = HexDigit( ( ( int ) line[ 0x0E ] & 0xF0 ) >> 4 );
		    sp.shot_time[ 3 ]   = HexDigit(   ( int ) line[ 0x0E ] & 0x0F	 );
		    sp.shot_time[ 4 ]   = HexDigit( ( ( int ) line[ 0x0F ] & 0xF0 ) >> 4 );
		    sp.shot_time[ 5 ]   = HexDigit(   ( int ) line[ 0x0F ] & 0x0F	 );

		    if ( sercel == SercelType.SERCEL_428 )
		    {
			//
			//	Time
			//

			sp.julian_day	=   ( ( int ) line[ 0x0B ] & 0x0F )	   * 100 +
					  ( ( ( int ) line[ 0x0C ] & 0xF0 ) >> 4 ) *  10 +
					    ( ( int ) line[ 0x0C ] & 0x0F );

			//
			//	GPS Time
			//

                        sp.gps_time = ( ByteToLong( line[ 0x05CC ] ) << 56 ) |
                                      ( ByteToLong( line[ 0x05CD ] ) << 48 ) |
                                      ( ByteToLong( line[ 0x05CE ] ) << 40 ) |
                                      ( ByteToLong( line[ 0x05CF ] ) << 32 ) |
                                      ( ByteToLong( line[ 0x05D0 ] ) << 24 ) |
                                      ( ByteToLong( line[ 0x05D1 ] ) << 16 ) |
                                      ( ByteToLong( line[ 0x05D2 ] ) <<  8 ) |
                                        ByteToLong( line[ 0x05D3 ] );
                    }

		    //
		    //	    Record Length
		    //

		    sp.record_length =  ( ByteToInt( line[ 0x260 ] ) << 24 ) |
					( ByteToInt( line[ 0x261 ] ) << 16 ) |
					( ByteToInt( line[ 0x262 ] ) <<  8 ) |
					  ByteToInt( line[ 0x263 ] );

		    //
		    //	    Uphole Time
		    //

		    iVal    = ( ByteToInt( line[ 0x02A4 ] ) << 24 ) |
                              ( ByteToInt( line[ 0x02A5 ] ) << 16 ) |
                              ( ByteToInt( line[ 0x02A6 ] ) <<  8 ) |
                                ByteToInt( line[ 0x02A7 ] );

                    sp.uphole_time   =	( short ) Math.round( ( double ) iVal / 1000.0 );

		    //
		    //	    Source Type
		    //

		    sVal =	( short ) ( ( ByteToInt( line[ 0x027C ] ) << 24 ) |
					    ( ByteToInt( line[ 0x027D ] ) << 16 ) |
					    ( ByteToInt( line[ 0x027E ] ) <<  8 ) |
					      ByteToInt( line[ 0x027F ] ) );

		    if ( sVal == 2 )
		    {
			sp.source_type[ 0 ] = 'V';
			sp.source_type[ 1 ] = '1';
		    }
		    else
		    {
			sp.source_type[ 0 ] = 'E';
			sp.source_type[ 1 ] = '1';
		    }

		    //
		    //	    Source Easting
		    //

		    lVal =  ( ByteToLong( line[ 0x049C ] ) << 56 ) |
			    ( ByteToLong( line[ 0x049D ] ) << 48 ) |
			    ( ByteToLong( line[ 0x049E ] ) << 40 ) |
			    ( ByteToLong( line[ 0x049F ] ) << 32 ) |
			    ( ByteToLong( line[ 0x04A0 ] ) << 24 ) |
			    ( ByteToLong( line[ 0x04A1 ] ) << 16 ) |
			    ( ByteToLong( line[ 0x04A2 ] ) <<  8 ) |
			      ByteToLong( line[ 0x04A3 ] );

		    sp.cog_easting =	Double.longBitsToDouble( lVal );

		    //
		    //	    Source Northing
		    //

		    lVal =  ( ByteToLong( line[ 0x04A4 ] ) << 56 ) |
			    ( ByteToLong( line[ 0x04A5 ] ) << 48 ) |
			    ( ByteToLong( line[ 0x04A6 ] ) << 40 ) |
			    ( ByteToLong( line[ 0x04A7 ] ) << 32 ) |
			    ( ByteToLong( line[ 0x04A8 ] ) << 24 ) |
			    ( ByteToLong( line[ 0x04A9 ] ) << 16 ) |
			    ( ByteToLong( line[ 0x04AA ] ) <<  8 ) |
			      ByteToLong( line[ 0x04AB ] );

		    sp.cog_northing =	Double.longBitsToDouble( lVal );

		    //
		    //	    Source Elevation
		    //

		    iVal =  ( ByteToInt( line[ 0x04AC ] ) << 24 ) |
			    ( ByteToInt( line[ 0x04AD ] ) << 16 ) |
			    ( ByteToInt( line[ 0x04AE ] ) <<  8 ) |
			      ByteToInt( line[ 0x04AF ] );

		    sp.cog_elevation =	Float.intBitsToFloat( iVal );

		    //
		    //	    Tape Number
		    //

		    sp.tape_number =	( ByteToInt( line[ 0x047C ] ) << 24 ) |
					( ByteToInt( line[ 0x047D ] ) << 16 ) |
					( ByteToInt( line[ 0x047E ] ) <<  8 ) |
					  ByteToInt( line[ 0x047F ] );

		    //
		    //	    Total Samples
		    //

		    nSampleRate	 =  ( ByteToInt( line[ 0x264 ] ) << 24 ) |
				    ( ByteToInt( line[ 0x265 ] ) << 16 ) |
				    ( ByteToInt( line[ 0x266 ] ) <<  8 ) |
				      ByteToInt( line[ 0x267 ] );

		    sp.total_samples =  ( sp.record_length * 1000 ) / nSampleRate;
		}

		//
		//	process the user header.
		//

		fis.skip( 138 );

		try
		{
		    if ( ( content = fis.read( usrhdr, 0, 1024 ) ) != -1 )
		    {
			if ( ( usrhdr != null ) && ( sp != null ) )
			{
			    strHdr  = new String( usrhdr );

			    if ( sercel == SercelType.SERCEL_408 )
			    {
				ProcessUserHeader( strHdr, sp.hdr );

				if ( hdrOK )
				{
				    QCData( sp );
				}
			    }

			    //
			    //	    determine if this is air gun data.
			    //

			    if ( ( strHdr.indexOf( "*LNGSH" ) != -1 ) ||
				 ( strHdr.indexOf( "*GCS90" ) != -1 ) )
			    {
				sp.source_type[ 0 ] = 'A';
			    }
			}
		    }
		}
		catch ( IOException e )
		{
		    textarea.append( "Error reading user header: " + e + "\n" );
		}
	    }
	    catch ( IOException e )
	    {
		textarea.append( "Error reading SEGD data: " + e + "\n" );
	    }

	    try
	    {
		fis.close();
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error closing input file: " + e );
	    }

	    return sp;
	}


	//
	//	decode the user header information.
	//

	private int ByteToInt( byte val )
	{
	    return ( ( int ) val & 0x000000FF );
	}

	private long ByteToLong( byte val )
	{
	    return ( ( long ) val & 0x000000FFL );
	}

	//
	//	decode the user header information.
	//

	private void ProcessUserHeader( String strHdr, HeaderParam hdr )
	{
	    int	    nIndex;
	    String  strSPS;
	    String  strGCS90;
	    Matcher matcher;

	    Pattern pattern = Pattern.compile( "\\d+" );

	    hdrOK   = false;

	    //
	    //	    Parse SPS header.
	    //

	    if ( ( nIndex = strHdr.indexOf( '*' ) ) == -1 )
	    {
		textarea.append( "End of SPS header not found.\n" );
		return;
	    }

	    strSPS  = strHdr.substring( 0, nIndex );

	    if ( ( nIndex = strSPS.indexOf( "SPS" ) ) == -1 )
	    {
		textarea.append( "Beginning of SPS header not found.\n" );
		return;
	    }

	    if ( strSPS.length() < 80 )
	    {
		textarea.append( "SPS header length error.\n" );
		return;
	    }

	    hdrOK   = true;
	    strSPS  = strSPS.substring( nIndex + 4 );

	    String  strToken;

	    if ( ( hdr.sps_line <= 0 ) && ( strSPS.charAt( 1 ) != '\0' ) )
	    {
		strToken    = strSPS.substring( 1, 11 );

		matcher = pattern.matcher( strToken );

		if ( matcher.find() )
		{
		    strToken = matcher.group();

		    try
		    {
			hdr.sps_line = Float.parseFloat( strToken );
		    }
		    catch ( NumberFormatException e )
		    {
			textarea.append( "Error decoding SPS line number: " + e + "\n" );

			hdr.sps_line = 0;
		    }
		}
		else
		{
		    textarea.append( "Error decoding SPS line number\n" );

		    hdr.sps_line = 0;
		}
	    }

	    if ( ( hdr.sps_station <= 0 ) && ( strSPS.charAt( 11 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 11, 25 );

		try
		{
		    hdr.sps_station  = Float.parseFloat( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS station number: " + e + "\n" );

		    hdr.sps_station = 0;
		}
	    }

	    if ( ( hdr.point_index <= 0 ) && ( strSPS.charAt( 25 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 25, 26 );

		try
		{
		    hdr.point_index  = Short.parseShort( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS point_index: " + e + "\n" );

		    hdr.point_index = 0;
		}
	    }

	    if ( ( hdr.source_type[ 0 ] == '\0' ) && ( strSPS.charAt( 26 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 26, 28 );

		strToken.getChars( 0, 2, hdr.source_type, 0 );
	    }

	    if ( ( hdr.uphole_time <= 0 ) && ( strSPS.charAt( 42 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 42, 46 );

		try
		{
		    hdr.uphole_time  = ( short ) ( Float.parseFloat( strToken ) + 0.5 );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS uphole_time: " + e + "\n" );

		    hdr.uphole_time = ( short ) 0;
		}
	    }

	    if ( ( hdr.cog_easting <= 0 ) && ( strSPS.charAt( 46 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 46, 55 );

		try
		{
		    hdr.cog_easting  = Double.parseDouble( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS cog_easting: " + e + "\n" );

		    hdr.cog_easting  = 0.0;
		}
	    }

	    if ( ( hdr.cog_northing <= 0 ) && ( strSPS.charAt( 55 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 55, 65 );

		try
		{
		    hdr.cog_northing  = Double.parseDouble( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS cog_northing: " + e + "\n" );

		    hdr.cog_northing  = 0.0;
		}
	    }

	    if ( ( hdr.cog_elevation <= 0 ) && ( strSPS.charAt( 65 ) != '\0' ) )
	    {
		strToken	    = strSPS.substring( 65, 71 );

		try
		{
		    hdr.cog_elevation  = Double.parseDouble( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS cog_elevation: " + e + "\n" );

		    hdr.cog_elevation  = 0.0;
		}
	    }

	    if ( ( hdr.julian_day <= 0 ) && ( strSPS.charAt( 71 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 71, 74 );

		try
		{
		    hdr.julian_day   = Integer.parseInt( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS julian_day: " + e + "\n" );

		    hdr.julian_day   = 0;
		}
	    }

	    if ( strSPS.charAt( 74 ) != '\0' )
	    {
		strToken	= strSPS.substring( 74, 80 );

		strToken.getChars( 0, 6, hdr.shot_time, 0 );
	    }

	    if ( ( hdr.hour <= 0 ) && ( strSPS.charAt( 74 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 74, 76 );

		try
		{
		    hdr.hour = Short.parseShort( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS hour: " + e + "\n" );

		    hdr.hour = 0;
		}
	    }

	    if ( ( hdr.minute <= 0 ) && ( strSPS.charAt( 76 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 76, 78 );

		try
		{
		    hdr.minute = Short.parseShort( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS minute: " + e + "\n" );

		    hdr.minute = 0;
		}
	    }

	    if ( ( hdr.second <= 0 ) && ( strSPS.charAt( 78 ) != '\0' ) )
	    {
		strToken	= strSPS.substring( 78, 80 );

		try
		{
		    hdr.second = Short.parseShort( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS second: " + e + "\n" );

		    hdr.second = 0;
		}
	    }

	    if ( ( hdr.gps_time <= 0 ) && ( strSPS.charAt( 81 ) != '\0' ) )
	    {
		long llval;

		strToken	= strSPS.substring( 81 );

		try
		{
		    llval	= Long.parseLong( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding SPS gps_time: " + e + "\n" );

		    llval = 0;
		}

		hdr.gps_time = llval / 1000;
	    }

	    //
	    //	    Parse GCS90 header.
	    //

	    if ( ( nIndex = strHdr.indexOf( "*GCS90" ) ) == -1 )
	    {
		textarea.append( "Beginning of GCS90 header not found.\n" );
		return;
	    }

	    strGCS90  = strHdr.substring( nIndex, nIndex + 94 );

	    if ( ( hdr.src_line <= 0 ) && ( strGCS90.charAt( 10 ) != '\0' ) )
	    {
		strToken    = strGCS90.substring( 10, 18 );

		matcher = pattern.matcher( strToken );

		if ( matcher.find() )
		{
		    strToken = matcher.group();

		    try
		    {
			hdr.src_line = Float.parseFloat( strToken );
		    }
		    catch ( NumberFormatException e )
		    {
			textarea.append( "Error decoding GCS90 line number: " + e + "\n" );

			hdr.src_line = 0;
		    }
		}
		else
		{
		    textarea.append( "Error decoding GCS90 line number\n" );

		    hdr.src_line = 0;
		}
	    }

	    if ( ( hdr.src_station <= 0 ) && ( strGCS90.charAt( 18 ) != '\0' ) )
	    {
		strToken	= strGCS90.substring( 18, 28 );

		try
		{
		    hdr.src_station  = Float.parseFloat( strToken );
		}
		catch ( NumberFormatException e )
		{
		    textarea.append( "Error decoding GCS90 station number: " + e + "\n" );

		    hdr.src_station = 0;
		}
	    }
	}

	//
	//	generate CSV file output from Sercel raw file.
	//

	private boolean ProcessRawFile( File file, FileOutputStream fos ) throws IOException
	{
	    List< SercelParam > spList	= new ArrayList< SercelParam >();

	    int		    pcount  = -1;
	    int		    status  = EOF;
	    SercelParam	    sp	    = null;
	    boolean	    bSkip   = false;

	    BufferedReader  br;
	    String	    line;
	    String	    strToken;
	    int		    nIndex;
	    int		    nSpc;
	    float	    fVersion;
	    int		    nSampleRate;

	    try
	    {
		fis = new FileInputStream( file.getPath() );
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error opening input file: " + e );
	    }

	    br	= new BufferedReader( new InputStreamReader( fis ) );

	    while ( ( line = br.readLine() ) != null )
	    {
		status = EOF;

		if ( line.indexOf( "Observer_Report : [" ) == 0 )
		{
		    pcount  = 0;
		    sp	    = new SercelParam();

		    spList.add( sp );
		}

		//
		//	check for the correct version number for the selected
		//	data type.
		//

		if ( line.indexOf( "Version" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken    = line.substring( nIndex + 3 ).trim();
			strToken    = strToken.substring( 0, strToken.length() - 1 );

			try
			{
			    fVersion  = Float.parseFloat( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding version: " + e + "\n" );

			    fVersion  = 0.0f;
			}

			if ( ( ( sercel == SercelType.SERCEL_408 ) &&
			     ( ( fVersion < 8.1f ) || ( fVersion >= 9.9f ) ) ) )
			{
			    JOptionPane.showMessageDialog( null,
							   "Incorrect file type selected.",
							   "Warning",
							   JOptionPane.WARNING_MESSAGE );

			    bSkip   = true;
			    break;
			}
			else if ( ( ( sercel == SercelType.SERCEL_428 ) &&
				  ( ( fVersion < 4.0f ) || ( fVersion >= 5.9f ) ) ) )
			{
			    JOptionPane.showMessageDialog( null,
							   "Incorrect file type selected.",
							   "Warning",
							   JOptionPane.WARNING_MESSAGE );

			    bSkip   = true;
			    break;
			}
		    }
		}

		//
		//	parse the raw observer log data.
		//

		if ( sercel == SercelType.SERCEL_408 )
		{
		    if ( line.indexOf( "Shot_Nb" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken	= line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.file_number  = Integer.parseInt( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding shot_id: " + e + "\n" );

				sp.file_number  = 0;
			    }

			    pcount++;
			}
		    }

		    if ( line.indexOf( "Source_Point_Line" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken    = line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.line = Float.parseFloat( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding line number: " + e + "\n" );

				sp.line = 0;
			    }

			    pcount++;
			}
		    }

		    if ( line.indexOf( "Source_Point_Nb" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken    = line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.station  = Float.parseFloat( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding station number: " + e + "\n" );

				sp.station	= 0;
			    }

			    pcount++;
			}
		    }

		    if ( line.indexOf( "Source_Point_Index" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken	= line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.point_index  = Short.parseShort( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding point_index: " + e + "\n" );

				sp.point_index  = 0;
			    }

			    pcount++;
			}
		    }
		}
		else
		{
		    if ( line.indexOf( "File_Nb" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken	= line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.file_number  = Integer.parseInt( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding shot_id: " + e + "\n" );

				sp.file_number  = 0;
			    }

			    pcount++;
			}
		    }

		    if ( line.indexOf( "Line_Name" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken    = line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.line = Float.parseFloat( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding line number: " + e + "\n" );

				sp.line = 0;
			    }

			    pcount++;
			}
		    }

		    if ( line.indexOf( "Point_Number" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken    = line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.station  = Float.parseFloat( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding station number: " + e + "\n" );

				sp.station	= 0;
			    }

			    pcount++;
			}
		    }

		    if ( line.indexOf( "Point_Index" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken	= line.substring( nIndex + 2 ).trim();

			    try
			    {
				sp.point_index  = Short.parseShort( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding point_index: " + e + "\n" );

				sp.point_index  = 0;
			    }

			    pcount++;
			}
		    }
		}

		if ( line.indexOf( "Acq_Length" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			if ( ( nSpc = strToken.indexOf( ' ' ) ) != -1 )
			{
			    strToken	= strToken.substring( 0, nSpc );
			}

			try
			{
			    sp.acq_length	= Integer.parseInt( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding acq_length: " + e + "\n" );

			    sp.acq_length	= 0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Sample_Rate" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			if ( ( nSpc = strToken.indexOf( ' ' ) ) != -1 )
			{
			    strToken	= strToken.substring( 0, nSpc );
			}

			if ( ( nIndex = strToken.indexOf( "SR_" ) ) != -1 )
			{
			    if ( strToken.indexOf( "SR_1_MS" ) == 0 )
			    {
				nSampleRate = 1000;
			    }
			    else if ( strToken.indexOf( "SR_2_MS" ) == 0 )
			    {
				nSampleRate = 2000;
			    }
			    else if ( strToken.indexOf( "SR_4_MS" ) == 0 )
			    {
				nSampleRate = 4000;
			    }
//~~~			    else if ( strToken.indexOf( "SR_500_US" ) == 0 )
//~~~			    {
//~~~				nSampleRate = 500;
//~~~			    }
//~~~			    else if ( strToken.indexOf( "SR_250_US" ) == 0 )
//~~~			    {
//~~~				nSampleRate = 250;
//~~~			    }
			    else
			    {
				nSampleRate = 2000;
			    }

			    sp.total_samples	=  ( sp.acq_length * 1000 ) / nSampleRate;
			}
			else
			{
			    try
			    {
				nSampleRate	    = Integer.parseInt( strToken );
				sp.total_samples    = ( sp.acq_length * 1000 ) / nSampleRate;
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding Sample_Rate: " + e + "\n" );

				sp.total_samples    = 0;
			    }
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Record_Length" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			if ( ( nSpc = strToken.indexOf( ' ' ) ) != -1 )
			{
			    strToken	= strToken.substring( 0, nSpc );
			}

			try
			{
			    sp.record_length    = Integer.parseInt( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding record_length: " + e + "\n" );

			    sp.record_length    = 0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Uphole_Time" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			if ( ( nSpc = strToken.indexOf( ' ' ) ) != -1 )
			{
			    strToken	= strToken.substring( 0, nSpc );
			}

			try
			{
			    sp.uphole_time	= ( short ) ( Float.parseFloat( strToken ) + 0.5f );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding uphole_time: " + e + "\n" );

			    sp.uphole_time	= 0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Type_Of_Source" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			if ( strToken.indexOf( "VIBRO" ) == 0 )
			{
			    sp.source_type[ 0 ] = 'V';
			    sp.source_type[ 1 ] = '1';
			}
			else if ( strToken.indexOf( "EXPLO" ) == 0 )
			{
			    sp.source_type[ 0 ] = 'E';
			    sp.source_type[ 1 ] = '1';
			}
			else if ( ( strToken.indexOf( "IMPUL" ) == 0 ) &&
				  ( sercel == SercelType.SERCEL_408 ) )
			{
			    sp.source_type[ 0 ] = 'E';
			    sp.source_type[ 1 ] = '1';
			}
			else
			{
			    sp.source_type[ 0 ] = 'A';
			    sp.source_type[ 1 ] = '1';
			}

			pcount++;
		    }
		}

		if ( ( sercel == SercelType.SERCEL_428 ) &&
		     ( sp != null ) &&
		     ( sp.source_type[ 0 ] == 'V' ) )
		{
		    sp.uphole_time  = 0;
		}

		if ( line.indexOf( "Tape_Nb" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.tape_number	= Integer.parseInt( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding tape_number: " + e + "\n" );

			    sp.tape_number	= 0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Cog_Easting" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.cog_easting	= Double.parseDouble( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding cog_easting: " + e + "\n" );

			    sp.cog_easting	= 0.0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Cog_Northing" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.cog_northing	= Double.parseDouble( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding cog_northing: " + e + "\n" );

			    sp.cog_northing	= 0.0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Cog_Elevation" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.cog_elevation    = Double.parseDouble( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding cog_elevation: " + e + "\n" );

			    sp.cog_elevation    = 0.0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Cog_Deviation" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.cog_deviation    = Double.parseDouble( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding cog_deviation: " + e + "\n" );

			    sp.cog_deviation    = 0.0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Hour" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.hour    = Short.parseShort( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding hour: " + e + "\n" );

			    sp.hour    = 0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Minute" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.minute   = Short.parseShort( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding minute: " + e + "\n" );

			    sp.minute   = 0;
			}

			pcount++;
		    }
		}

		if ( line.indexOf( "Second" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken	= line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.second   = Short.parseShort( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding second: " + e + "\n" );

			    sp.second   = 0;
			}

			pcount++;
		    }
		}

		if ( sercel == SercelType.SERCEL_408 )
		{
		    strToken    = null;

		    if ( sp != null )
		    {
			try
			{
			    strToken	= String.format( "%02d%02d%02d, ", sp.hour, sp.minute, sp.second );
			}
			catch( IllegalFormatException e )
			{
			    textarea.append( "Error decoding shot_time: " + e + "\n" );

			    strToken	= "000000";
			}
			catch( NullPointerException e )
			{
			    textarea.append( "Error decoding shot_time: " + e + "\n" );

			    strToken	= "000000";
			}
			finally
			{
			    strToken.getChars( 0, 6, sp.shot_time, 0 );
			    pcount++;
			}
		    }
		}
		else
		{
		    if ( line.indexOf( "Date" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    strToken    = line.substring( nIndex + 2 ).trim();
			    strToken    = strToken.substring( 11, 13 ) +
					  strToken.substring( 14, 16 ) +
					  strToken.substring( 17, 19 );

			    strToken.getChars( 0, 6, sp.shot_time, 0 );
			    pcount++;
			}
		    }

		    if ( line.indexOf( "Tb_GPS_Time" ) != -1 )
		    {
			if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
			{
			    long llval;

			    strToken	= line.substring( nIndex + 2 ).trim();

			    try
			    {
				llval	= Long.parseLong( strToken );
			    }
			    catch ( NumberFormatException e )
			    {
				textarea.append( "Error decoding SPS gps_time: " + e + "\n" );

				llval = 0;
			    }

			    sp.gps_time = llval;
			    pcount++;
			}
		    }
		}

		if ( line.indexOf( "Julian_Day" ) != -1 )
		{
		    if ( ( nIndex = line.indexOf( ':' ) ) != -1 )
		    {
			strToken    = line.substring( nIndex + 2 ).trim();

			try
			{
			    sp.julian_day   = Integer.parseInt( strToken );
			}
			catch ( NumberFormatException e )
			{
			    textarea.append( "Error decoding julian_day: " + e + "\n" );

			    sp.julian_day   = 0;
			}

			pcount++;
		    }
		}

		//
		//	parse the user header.
		//

		if ( line.indexOf( "User_Header" ) != -1 )
		{
		    if ( sercel == SercelType.SERCEL_408 )
		    {
			ProcessUserHeader( line, sp.hdr );

			if ( hdrOK )
			{
			    QCData( sp );
			}

			pcount++;
		    }
		    else
		    {
			//
			//	determine if this is air gun data.
			//

			if ( ( line.indexOf( "*LNGSH" ) != -1 ) ||
			     ( line.indexOf( "*GCS90" ) != -1 ) )
			{
			    sp.source_type[ 0 ] = 'A';
			}
		    }
		}

		//
		//	update the progress indicator.
		//

		processed++;

		data		= new ProgressData();
		data.processed	= processed;

		publish( data );
	    }

	    //
	    //	    write the output.
	    //

	    if ( !bSkip )
	    {
		for ( SercelParam spo : spList )
		{
		    WriteCSVFile( spo, fos, settings.bFirstNine );
		}
	    }

	    try
	    {
		fis.close();
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error closing input file: " + e );
	    }

	    return !bSkip;
	}

	//
	//	compare the corresponding raw nav log or SegD fields
	//	with their user header equivalents.
	//

	private void QCData( SercelParam sp )
	{
	    if ( sp.line != sp.hdr.sps_line )
	    {
		textarea.append( String.format( "{L%10.2f-S%10.2f} line mismatch: [%10.2f/%10.2f]\n",
						sp.line,
						sp.station,
						sp.line,
						sp.hdr.sps_line ) );
	    }

	    if ( sp.station != sp.hdr.sps_station )
	    {
		textarea.append( String.format( "{L%10.2f-S%10.2f} station mismatch: [%10.2f/%10.2f]\n",
						sp.line,
						sp.station,
						sp.station,
						sp.hdr.sps_station ) );
	    }

	    if ( sp.point_index != sp.hdr.point_index )
	    {
		textarea.append( String.format( "{L%10.2f-S%10.2f} point index mismatch: [%1X/%1X]\n",
						sp.line,
						sp.station,
						sp.point_index,
						sp.hdr.point_index ) );
	    }

	    if ( ( input == InputType.INPUT_SEGD ) && ( sercel == SercelType.SERCEL_428 ) )
	    {
		if ( sp.gps_time != sp.hdr.gps_time )
		{
		    textarea.append( String.format( "{L%10.2f-S%10.2f} GPS time mismatch: [%16d/%16d]\n",
						    sp.line,
						    sp.station,
						    sp.gps_time,
						    sp.hdr.gps_time ) );
		}
	    }

	    if ( input == InputType.INPUT_RAW )
	    {
		if ( sercel == SercelType.SERCEL_428 )
		{
		    if ( sp.cog_easting != sp.hdr.cog_easting )
		    {
			textarea.append( String.format( "{L%10.2f-S%10.2f} cog easting mismatch: [%9.1f/%9.1f]\n",
							sp.line,
							sp.station,
							sp.cog_easting,
							sp.hdr.cog_easting ) );
		    }
	
		    if ( sp.cog_northing != sp.hdr.cog_northing )
		    {
			textarea.append( String.format( "{L%10.2f-S%10.2f} cog northing mismatch: [%10.1f/%10.1f]\n",
							sp.line,
							sp.station,
							sp.cog_northing,
							sp.hdr.cog_northing ) );
		    }
	
		    if ( sp.cog_elevation != sp.hdr.cog_elevation )
		    {
			textarea.append( String.format( "{L%10.2f-S%10.2f} cog elevation mismatch: [%6.1f/%6.1f]\n",
							sp.line,
							sp.station,
							sp.cog_elevation,
							sp.hdr.cog_elevation ) );
		    }
		}

		if ( sp.julian_day != sp.hdr.julian_day )
		{
		    textarea.append( String.format( "{L%10.2f-S%10.2f} julian day mismatch: [%3d/%3d]\n",
						    sp.line,
						    sp.station,
						    sp.julian_day,
						    sp.hdr.julian_day ) );
		}

		if ( ( sercel	      != SercelType.SERCEL_408	) &&
		     ( sp.uphole_time != sp.hdr.uphole_time	) )
		{
		    textarea.append( String.format( "{L%10.2f-S%10.2f} uphole time mismatch: [%2d/%2d]\n",
						    sp.line,
						    sp.station,
						    sp.uphole_time,
						    sp.hdr.uphole_time ) );
		}

		if ( ( sp.source_type[ 0 ] != sp.hdr.source_type[ 0 ] ) ||
		     ( sp.source_type[ 1 ] != sp.hdr.source_type[ 1 ] ) )
		{
		    textarea.append( String.format( "{L%10.2f-S%10.2f} source type mismatch: [%2s/%2s]\n",
						    sp.line,
						    sp.station,
						    ( new String( sp.source_type ) ).trim(),
						    ( new String( sp.hdr.source_type ) ).trim() ) );
		}
	    }
	}

	//
	//	generate CSV file output from nav file.
	//

	private void ProcessNavFile( File file, FileOutputStream fos )
	{
	    SercelParam sp = new SercelParam();

//~~~	    int count = 0;
//~~~
//~~~	    if ( ( inputfile == NULL ) || ( params == NULL ) )
//~~~		return 0;
//~~~
//~~~	    FILE* fIn = fopen( inputfile, "r" );
//~~~
//~~~	    if ( fIn == NULL )
//~~~		return 0;
//~~~
//~~~	    //
//~~~	    //	    parse out file header
//~~~	    //
//~~~
//~~~	    int linelen = 0;
//~~~	    char line[ 1024 ], Q[ 20 ], X[ 12 ], Y[ 12 ], time[ 17 ], date[ 7 ], df[ 2 ];
//~~~	    short date_format = 0;
//~~~	    uint64_t gpstime = 0;
//~~~
//~~~	    while( fgets( line, sizeof( line ), fIn ) )
//~~~	    {
//~~~		linelen = strlen( line );
//~~~
//~~~		if( !is_nav_record( line ) )
//~~~		    continue;
//~~~
//~~~		memset( Q, 0, sizeof( Q ) );
//~~~
//~~~		//
//~~~		//	Quality of header
//~~~		//
//~~~
//~~~		strncpy( Q, &( line[ 6 ] ), 2 );
//~~~		int quality = atoi( Q );
//~~~
//~~~		if( quality != 1 )
//~~~		    continue;
//~~~
//~~~		//
//~~~		//	Date Format (mm/dd/yy = 0, dd/mm/yy = 1)
//~~~		//
//~~~
//~~~		strncpy( df, &( line[ 14 ] ), 1 );
//~~~		date_format = atoi( df );
//~~~
//~~~		//
//~~~		//	HHMMSS.SSSSSSSSS
//~~~		//
//~~~
//~~~		strncpy( time, &( line[ 17 ] ), 16 );
//~~~		time[ 16 ] = '\0';
//~~~		strncpy( sp.shot_time, time, 6 );
//~~~		sp.shot_time[ 6 ] = '\0';
//~~~
//~~~		//
//~~~		//	MMDDYY or DDMMYY
//~~~		//
//~~~
//~~~		strncpy( date, &( line[ 33 ] ), 6 );
//~~~		date[ 6 ] = '\0';
//~~~
//~~~		gpstime = parse_gpstime( date, time, date_format );
//~~~		sp.gps_time = gpstime;
//~~~
//~~~		//
//~~~		//	Y
//~~~		//
//~~~
//~~~		strncpy( Y, &( line[ 70 ] ), 11 );
//~~~		sp.cog_northing = atof( Y );
//~~~
//~~~		//
//~~~		//	X
//~~~		//
//~~~
//~~~		strncpy( X, &( line[ 81 ] ), 11 );
//~~~		sp.cog_easting = atof( X );
//~~~
//~~~		//
//~~~		//	Water Depth
//~~~		//
//~~~
//~~~		strncpy( X, &( line[ 117 ] ), 8 );
//~~~
//~~~		count++;
//~~~	    }
//~~~
//~~~	    fclose( fIn );
//~~~
//~~~	    return count;
	}

	//
	//	output data to file.
	//

	private void WriteCSVFile( SercelParam sp,
				    FileOutputStream fos,
				    boolean bFirstNine )
	{
	    StringBuilder   sb = new StringBuilder();
	    long	    lTemp;

	    //
	    //	    # Line
	    //

	    sb.append( String.format( "%10.2f, ", sp.line ) );

	    //
	    //	    Station
	    //

	    sb.append( String.format( "%10.2f, ", sp.station ) );

	    //
	    //	    Shot Time
	    //

	    if ( sercel == SercelType.SERCEL_408 )
	    {
		if ( ( sp.hdr.shot_time.length <= 0 ) || ( sp.hdr.shot_time[ 0 ] == '\0' ) )
		{
		    if ( sp.hdr.julian_day <= 0 )
		    {
			sb.append( String.format( "%03d%02d%02d%02d, ",
						  sp.julian_day,
						  sp.hdr.hour,
						  sp.hdr.minute,
						  sp.hdr.second ) );
		    }
		    else
			sb.append( String.format( "%03d%02d%02d%02d, ",
						  sp.hdr.julian_day,
						  sp.hdr.hour,
						  sp.hdr.minute,
						  sp.hdr.second ) );
		}
		else
		{
		    if ( sp.hdr.julian_day <= 0 )
		    {
			sb.append( String.format( "%03d", sp.julian_day ) );
		    }
		    else
			sb.append( String.format( "%03d", sp.hdr.julian_day ) );

		    sb.append( Arrays.copyOf( sp.hdr.shot_time, 6 ) );
		    sb.append( ", " );
		}
	    }
	    else
	    {
		if ( sp.shot_time.length <= 0 )
		{
		    if ( ( sp.hour <= 0 ) && ( sp.minute <= 0 ) && ( sp.second <= 0 ) )
		    {
			if ( sp.hdr.shot_time.length <= 0 )
			{
			    sb.append( String.format( "%03d%02d%02d%02d, ",
						      sp.julian_day,
						      sp.hdr.hour,
						      sp.hdr.minute,
						      sp.hdr.second ) );
			}
			else
			{
			    sb.append( String.format( "%03d", sp.julian_day ) );
			    sb.append( Arrays.copyOf( sp.hdr.shot_time, 6 ) );
			    sb.append( ", " );
			}
		    }
		    else
		    {
			sb.append( String.format( "%03d%02d%02d%02d, ",
						  sp.julian_day,
						  sp.hour,
						  sp.minute,
						  sp.second ) );
		    }
		}
		else
		{
		    sb.append( String.format( "%03d", sp.julian_day ) );
		    sb.append( Arrays.copyOf( sp.shot_time, 6 ) );
		    sb.append( ", " );
		}
	    }

	    //
	    //	    GPS Time 
	    //

	    lTemp  = ( sp.gps_time <= 0 ) ? sp.hdr.gps_time : sp.gps_time;

	    switch ( sp.source_type[ 0 ] )
	    {
		case 'E'    :

		    lTemp += ( long ) ( bAdjustTime ? settings.fDynamiteOffset * 1000.0f : 0.0f );
		    break;

		case 'V'    :

		    lTemp += ( long ) ( bAdjustTime ? settings.fVibratorOffset * 1000.0f : 0.0f );
		    break;

		case 'A'    :

		    lTemp += ( long ) ( bAdjustTime ? settings.fAirGunOffset * 1000.0f : 0.0f );
		    break;

		default	    :

		    break;
	    }

	    sb.append( String.format( "%16d, ", lTemp ) );
 
	    //
	    //	    File Number
	    //

	    sb.append( String.format( "%5d, ", sp.file_number ) );

	    //
	    //	    Record Length
	    //

	    int reclen = ( sp.acq_length > 0 ) ? sp.acq_length : sp.record_length;

	    sb.append( String.format( "%5d, ", reclen ) );

	    //
	    //	    Point Index
	    //

	    sb.append( String.format( "%1X, ", ( ( sp.point_index <= 0 ) && ( sercel == SercelType.SERCEL_408 ) ) ? sp.hdr.point_index : sp.point_index ) );

	    //
	    //	    Uphole Time
	    //

	    sb.append( String.format( "%2d, ", sp.uphole_time ) );

	    //
	    //	    Source Type
	    //

	    sb.append( ( sp.source_type[ 0 ] != '\0' ) ? sp.source_type[ 0 ] : ' ' );
	    sb.append( ( sp.source_type[ 1 ] != '\0' ) ? sp.source_type[ 1 ] : ' ' );

	    if ( !bFirstNine )
	    {
		if ( sercel == SercelType.SERCEL_428 )
		{
		    sb.append( ", " );

		    //
		    //	    Easting
		    //

		    sb.append( String.format( "%9.1f, ", sp.cog_easting ) );

		    //
		    //	    Northing
		    //

		    sb.append( String.format( "%10.1f, ", sp.cog_northing ) );

		    //
		    //	    Elevation
		    //

		    sb.append( String.format( "%6.1f, ", sp.cog_elevation ) );

		    //
		    //	    Deviation
		    //

		    sb.append( String.format( "%6.1f, ", sp.cog_deviation ) );

		    //
		    //	    Tape Number
		    //

		    sb.append( String.format( "%3d,", sp.tape_number ) );

		    //
		    //	    Total Samples;
		    //

		    sb.append( String.format( "%6d", sp.total_samples ) );
		}
	    }

	    //
	    //	    output completed CSV line.
	    //

	    sb.append( "\n" );

	    try
	    {
		fos.write( sb.toString().getBytes() );
	    }
	    catch ( IOException e )
	    {
		System.err.println( "Error writing to output file: " + e );
	    }
	}

//~~~	uint64_t parse_gpstime( char* date, char* time, uint8_t date_format )
//~~~	{
//~~~	    uint64_t gpstime = 0;
//~~~	    char t1[ 3 ], t2[ 10 ];
//~~~	    short hh,mm,ss;
//~~~	    uint32_t nanosecs;
//~~~	    short day, month, year;
//~~~
//~~~	    //
//~~~	    //	    GPS starting date 01/06/1980
//~~~	    //
//~~~
//~~~	    if ( ( date == NULL ) || ( time == NULL ) )
//~~~		return 0;
//~~~
//~~~	    if ( ( strlen( date ) < 6 ) || ( strlen( time ) < 9 ) )
//~~~		return 0;
//~~~
//~~~	    strncpy( t1, date, 2 );
//~~~
//~~~	    if ( date_format != 0 )
//~~~		day = atoi( t1 );
//~~~	    else
//~~~		month = atoi( t1 );
//~~~
//~~~	    strncpy( t1, &( date[ 2 ] ), 2 );
//~~~
//~~~	    if ( date_format != 0 )
//~~~		month = atoi( t1 );
//~~~	    else
//~~~		day = atoi( t1 );
//~~~
//~~~	    strncpy( t1, &( date[ 4 ] ), 2 );
//~~~	    year = atoi( t1 );
//~~~	    year += 2000;
//~~~
//~~~	    strncpy( t1, time, 2 );
//~~~	    hh = atoi( t1 );
//~~~
//~~~	    strncpy( t1, &( time[ 2 ] ), 2 );
//~~~	    mm = atoi( t1 );
//~~~
//~~~	    strncpy( t1, &( time[ 4 ] ), 2 );
//~~~	    ss = atoi( t1 );
//~~~
//~~~	    strncpy( t2, &( time[ 7 ] ), 9 );
//~~~	    nanosecs = ( uint32_t ) atoi( t2 );
//~~~
//~~~	    //
//~~~	    //	    test
//~~~	    //
//~~~
//~~~	    struct tm dt;
//~~~	    memset( &dt, 0, sizeof( struct tm ) );
//~~~
//~~~	    dt.tm_year = year-1980;
//~~~	    dt.tm_mon = month-1;
//~~~	    dt.tm_mday = day;
//~~~	    dt.tm_hour = hh;
//~~~	    dt.tm_min = mm;
//~~~	    dt.tm_sec = ss;
//~~~	    dt.tm_isdst = -1;
//~~~	    uint32_t secs = ( uint32_t ) timegm( &dt );
//~~~
//~~~	    gpstime = ( uint64_t ) ( dt.tm_yday * 24 * 60 * 60 ) * 1000000000 + nanosecs;
//~~~
//~~~	    gpstime /= 1000;
//~~~
//~~~	    return gpstime;
//~~~	}

//~~~	int is_nav_record( char* buffer )
//~~~	{
//~~~	    if ( buffer == NULL )
//~~~		return 0;
//~~~
//~~~	    char* p1 = strstr( buffer, "NCS" );
//~~~	    char* p2 = strstr( buffer, "NAV" );
//~~~
//~~~	    if ( ( p1 != NULL ) && ( p2 != NULL ) )
//~~~		return 1;
//~~~
//~~~	    return 0;
//~~~	}

	private char HexDigit( int nib )
	{
	    return ( nib < 0x0A ) ? ( char ) ( nib + 0x30 ) : ( char ) ( nib + 0x37 );
	}

	private FileInputStream fis;
	private FileOutputStream fos;
	private File[] files;
	private String strOutput;
	private SercelType sercel;
	private InputType input;
	private ProgressData data;
	private JProgressBar progressbar;
	private MyTextArea textarea;
	private int processed;
	private boolean hdrOK;
	private boolean bAdjustTime;
	private TBGenSettings settings;
}

//
//	comparator classes.
//

class GPSTimeComparator implements Comparator< SercelParam > 
{
	@Override
	public int compare( SercelParam o1, SercelParam o2 )
	{
	    if ( o1.gps_time == o2.gps_time )
	    {
		return 0;
	    }

	    return ( o1.gps_time < o2.gps_time ) ? -1 : 1;
	}
}

class FileNameComparator implements Comparator< File > 
{
	@Override
	public int compare( File o1, File o2 )
	{
	    return o1.getName().compareTo( o2.getName() );
	}
}

//
//	end of FileReader.java.
//
