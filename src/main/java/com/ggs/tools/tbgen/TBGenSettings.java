//
//	module	      :	TBGenSettings.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the TBGenSettings class for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	22apr2013	jeff thomson	new code.
//	27sep2013	jeff thomson	add bFirstSeven.
//	31oct2013	jeff thomson	change bFirstSeven to bFirstEight.
//	04nov2013	jeff thomson	change bFirstEight to bFirstNine.
//

package com.ggs.tools.tbgen;


public class TBGenSettings
{
	public TBGenSettings()
	{
	    fDynamiteOffset	= 0.0f;
	    fVibratorOffset	= 0.0f;
	    fAirGunOffset	= 0.0f;
	    strProjectName	= "";
	    bFirstNine		= false;
	}

	public float	fDynamiteOffset;
	public float	fVibratorOffset;
	public float	fAirGunOffset;
	public String	strProjectName;
	public boolean	bFirstNine;
}

//
//	end of TBGenSettings.java.
//
