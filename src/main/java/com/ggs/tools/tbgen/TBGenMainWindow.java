//
//	module	      :	TBGenMainWindow.java
//
//	program	      :	TBGen
//
//	target system :	any computer running the Java Runtime
//			Environment (JRE) version 6 or later.
//
//	purpose	      :	implements the main user interface for the
//			TBGen application.
//
//	notes	      :	none.
//
//	language      :	Java
//
//	history	      :
//
//	date		authors		comments
//
//	15jan2013	jeff thomson	new code.
//	14feb2013	jeff thomson	extract supporting classes to
//					separate modules.
//	20feb2013	jeff thomson	add version string to title bar;
//					append "_output.csv" to input
//					file name.
//	22feb2013	jeff thomson	remove output file name parameter
//					from argument list of FileReader
//					constructor;
//					for now, disable Nav file radio
//					button.
//	26feb2013	jeff thomson	add support for message logging
//					using class derived from
//					JTextArea.
//	05mar2013	jeff thomson	update version number.
//	07mar2013	jeff thomson	fix bug in SEGD file handling.
//	11mar2013	jeff thomson	update version number.
//	11mar2013	jeff thomson	update version number.
//	14mar2013	jeff thomson	update version number;
//                                      modify to use log dir setter.
//	21mar2013	jeff thomson	update version number.
//	22mar2013	jeff thomson	update version number.
//	27mar2013	jeff thomson	update version number.
//	02apr2013	jeff thomson	update version number.
//	08apr2013	jeff thomson	update version number.
//	10apr2013	jeff thomson	update version number.
//	12apr2013	jeff thomson	move window closing event handler here
//					from application class;
//					add support for saving/restoring
//					application settings.
//	16apr2013	jeff thomson	add support for GPS time adjustment.
//	19apr2013	jeff thomson	add support for project name and per-
//					source type GPS time adjustment.
//	22apr2013	jeff thomson	pass project file name to text area
//					object to generate unique log file
//					names.
//	22apr2013	jeff thomson	update version number.
//	23apr2013	jeff thomson	force user to input project name.
//	25apr2013	jeff thomson	update version number.
//	29apr2013	jeff thomson	add support for using enter key to press
//					"File Open..." and "Settings..." buttons.
//	02may2013	jeff thomson	update version number.
//	02may2013	jeff thomson	update version number.
//	06may2013	jeff thomson	update version number.
//	13may2013	jeff thomson	add Run button.
//	15may2013	jeff thomson	modify file open dialog box to display
//					SEGD files.
//	17may2013	jeff thomson	use different default font.
//	19may2013	jeff thomson	fix resizable bug.
//	27aug2013	jeff thomson	add support for "help" button.
//	27sep2013	jeff thomson	disable "Run" button until files have
//					been selected.
//	27sep2013	jeff thomson	update version number.
//	08oct2013	jeff thomson	fix button sizing issues on Linux.
//	09oct2013	jeff thomson	fix bug where help file was not being
//					located correctly.
//	17oct2013	jeff thomson	fix file open dialog filter bug.
//	31oct2013	jeff thomson	change bFirstSeven to bFirstEight;
//					fix file chooser directory retention
//					bug.
//	04nov2013	jeff thomson	change bFirstEight to bFirstNine.
//	16dec2013	jeff thomson	update version number.
//	03mar2014	jeff thomson	update version number.
//	04mar2014	jeff thomson	update version number.
//	10mar2014	jeff thomson	update version number.
//	24apr2014	jeff thomson	update version number.
//	30sep2014	jeff thomson	update version number.
//	14oct2014	jeff thomson	update version number.
//

package com.ggs.tools.tbgen;

import java.awt.Color;
import java.awt.Desktop;
import java.io.*;
import java.util.*;
import javax.swing.JFileChooser;
import java.util.prefs.*;
import java.net.*;

public class TBGenMainWindow extends javax.swing.JFrame
{
	private static final long serialVersionUID = -3458989714400949590L;
	public enum SercelType
	{
	    SERCEL_408,
	    SERCEL_428;
	}

	public enum InputType
	{
	    INPUT_SEGD,
	    INPUT_RAW,
	    INPUT_NAV;
	}

	private TBGenSettings	settings;
	private JFileChooser	fc;
	private SercelType	sercel;
	private InputType	input;
	private boolean		bAdjustTime;
	private Preferences	root;
	private Preferences	node;
	private File		file;
	private File[]		files;

	private static final String strVersion	= "1.0.46";

	//
	//      Creates new form TBGenMainWindow
	//

	public TBGenMainWindow( java.awt.Frame parent, boolean modal )
	{
	    initComponents();

	    //
	    //	    Create a file chooser.
	    //

	    fc = new JFileChooser();

	    //
	    //	    Read the settings
	    //

	    root	= Preferences.userRoot();
	    node	= root.node( "/pbxsys/tbgen" );
	    settings	= new TBGenSettings();

	    sercel			= SercelType.values()[ node.getInt( "sercel", SercelType.SERCEL_408.ordinal() ) ];
	    input			= InputType.values()[ node.getInt( "input", InputType.INPUT_SEGD.ordinal() ) ];
	    bAdjustTime			= node.getBoolean( "adjusttime", false );
	    settings.fDynamiteOffset	= node.getFloat( "dynamiteoffset", 0.0f );
	    settings.fVibratorOffset	= node.getFloat( "vibratoroffset", 0.0f );
	    settings.fAirGunOffset	= node.getFloat( "airgunoffset", 0.0f );
	    settings.strProjectName	= node.get( "projectname", "" );
	    settings.bFirstNine		= node.getBoolean( "firstnine", false );

	    //
	    //	    Handle window close event
	    //

	    this.addWindowListener( new java.awt.event.WindowAdapter()
				    {
					@Override
					public void windowClosing( java.awt.event.WindowEvent e )
					{
					    node.putInt( "sercel", sercel.ordinal() );
					    node.putInt( "input", input.ordinal() );
					    node.putBoolean( "adjusttime", bAdjustTime );
					    node.putFloat( "dynamiteoffset", settings.fDynamiteOffset );
					    node.putFloat( "vibratoroffset", settings.fVibratorOffset );
					    node.putFloat( "airgunoffset", settings.fAirGunOffset );
					    node.put( "projectname", settings.strProjectName );
					    node.putBoolean( "firstnine", settings.bFirstNine );

					    //System.exit( 0 );
					}
				    } );

	    //
	    //	    initialize the inputs.
	    //

	    switch ( sercel )
	    {
		case SERCEL_428	:

		    jRadioButtonSercel428.setSelected( true );
		    break;

		case SERCEL_408	:
		default		:

		    jRadioButtonSercel408.setSelected( true );
		    break;
	    }

	    switch ( input )
	    {
		case INPUT_NAV	:

		    jRadioButtonNav.setSelected( true );
		    break;

		case INPUT_RAW	:

		    jRadioButtonSercelRaw.setSelected( true );
		    break;

		case INPUT_SEGD	:
		default		:

		    jRadioButtonSEGD.setSelected( true );
		    break;
	    }

	    jCheckBoxAdjustTime.setSelected( bAdjustTime );
	    jCheckBoxAdjustTime.setForeground( bAdjustTime ? Color.red : Color.black );
	
	    this.setTitle( "TBGen " + strVersion + ( ( settings.strProjectName.length() > 0 ) ? " [" + settings.strProjectName + "]" : "" ) );

	    if ( settings.strProjectName.length() == 0 )
	    {
		jButtonSettingsMouseClicked( null );
	    }
	}

	//
	//      the "Open File" button was clicked.
	//

	private void jButtonOpenFileMouseClicked( java.awt.event.MouseEvent evt )
	{//GEN-FIRST:event_jButton2MouseClicked
	    //
	    //	    reset the file filters, so they don't accumulate following
	    //	    multiple invocations of the file chooser dialog.
	    //

	    fc.resetChoosableFileFilters();

	    fc.setFileSelectionMode( JFileChooser.FILES_ONLY );
	    fc.setMultiSelectionEnabled( true );

	    if ( input == InputType.INPUT_SEGD )
	    {
		JFileFilter ff	= new JFileFilter();

		ff.addType( ".segd" );
		ff.addType( ".sgd"  );
		ff.addType( ".seg"  );
		ff.setDescription( "SEGD files" );
		
		fc.setFileFilter( ff );
	    }
		
	    int returnVal = fc.showOpenDialog( TBGenMainWindow.this );

	    if ( returnVal == JFileChooser.APPROVE_OPTION )
	    {
		file	= fc.getSelectedFile();
		files	= fc.getSelectedFiles();

		if ( ( files.length != 0 ) ||
		       file.isFile()  ||
		     ( file.isDirectory() && ( file.listFiles().length != 0 ) ) )
		{
		    jButtonRun.setEnabled( true );
		}
	    }
	    else
	    {
		jTextArea1.append( "Open command cancelled by user." + "\n" );
	    }
	}//GEN-LAST:event_jButton2MouseClicked

	//
	//      count the number of lines in the specified file.
	//

	private int CountLines( String filename ) throws IOException
	{
	    InputStream is = new BufferedInputStream( new FileInputStream( filename ) );

	    try
	    {
		byte[] c = new byte[ 1024 ];
		int count = 0;
		int readChars = 0;
		boolean empty = true;

		while ( ( readChars = is.read( c ) ) != -1 )
		{
		    empty = false;

		    for ( int i = 0; i < readChars; ++i )
		    {
			if ( c[ i ] == '\n' )
			{
			    ++count;
			}
		    }
		}

		return ( ( count == 0 ) && !empty ) ? 1 : count;
	    }
	    finally
	    {
		is.close();
	    }
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jProgressBar1 = new javax.swing.JProgressBar();
        jPanel1 = new javax.swing.JPanel();
        jRadioButtonSercel428 = new javax.swing.JRadioButton();
        jRadioButtonSercel408 = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jRadioButtonSEGD = new javax.swing.JRadioButton();
        jRadioButtonSercelRaw = new javax.swing.JRadioButton();
        jRadioButtonNav = new javax.swing.JRadioButton();
        jButtonOpenFile = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new MyTextArea();
        jCheckBoxAdjustTime = new javax.swing.JCheckBox();
        jButtonSettings = new javax.swing.JButton();
        jButtonRun = new javax.swing.JButton();
        jButtonHelp = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(555, 560));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Input Type", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Arial", 1, 14))); // NOI18N

        buttonGroup1.add(jRadioButtonSercel428);
        jRadioButtonSercel428.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jRadioButtonSercel428.setSelected(true);
        jRadioButtonSercel428.setText("Sercel 428");
        jRadioButtonSercel428.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonSercel428ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButtonSercel408);
        jRadioButtonSercel408.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jRadioButtonSercel408.setText("Sercel 408");
        jRadioButtonSercel408.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonSercel408ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jRadioButtonSercel428)
                .addGap(18, 18, 18)
                .addComponent(jRadioButtonSercel408)
                .addContainerGap(9, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonSercel428)
                    .addComponent(jRadioButtonSercel408))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Input Data", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Arial", 1, 14))); // NOI18N

        buttonGroup2.add(jRadioButtonSEGD);
        jRadioButtonSEGD.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jRadioButtonSEGD.setSelected(true);
        jRadioButtonSEGD.setText("SEG D");
        jRadioButtonSEGD.setMaximumSize(new java.awt.Dimension(111, 33));
        jRadioButtonSEGD.setMinimumSize(new java.awt.Dimension(111, 33));
        jRadioButtonSEGD.setPreferredSize(new java.awt.Dimension(111, 33));
        jRadioButtonSEGD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonSEGDActionPerformed(evt);
            }
        });

        buttonGroup2.add(jRadioButtonSercelRaw);
        jRadioButtonSercelRaw.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jRadioButtonSercelRaw.setText("Sercel Raw Obs. Log");
        jRadioButtonSercelRaw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonSercelRawActionPerformed(evt);
            }
        });

        buttonGroup2.add(jRadioButtonNav);
        jRadioButtonNav.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jRadioButtonNav.setText("Nav");
        jRadioButtonNav.setEnabled(false);
        jRadioButtonNav.setMaximumSize(new java.awt.Dimension(111, 33));
        jRadioButtonNav.setMinimumSize(new java.awt.Dimension(111, 33));
        jRadioButtonNav.setPreferredSize(new java.awt.Dimension(111, 33));
        jRadioButtonNav.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonNavActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jRadioButtonSEGD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jRadioButtonSercelRaw)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jRadioButtonNav, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonSEGD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButtonSercelRaw)
                    .addComponent(jRadioButtonNav, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonOpenFile.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jButtonOpenFile.setLabel("Open File(s)...");
        jButtonOpenFile.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonOpenFileMouseClicked(evt);
            }
        });
        jButtonOpenFile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jButtonOpenFileKeyTyped(evt);
            }
        });

        jTextArea1.setEditable(false);
        jTextArea1.setBackground(new java.awt.Color(240, 240, 240));
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jCheckBoxAdjustTime.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jCheckBoxAdjustTime.setText("Adjust Time");
        jCheckBoxAdjustTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxAdjustTimeActionPerformed(evt);
            }
        });

        jButtonSettings.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jButtonSettings.setText("Settings...");
        jButtonSettings.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonSettingsMouseClicked(evt);
            }
        });
        jButtonSettings.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jButtonSettingsKeyTyped(evt);
            }
        });

        jButtonRun.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jButtonRun.setText("Run");
        jButtonRun.setEnabled(false);
        jButtonRun.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonRunMouseClicked(evt);
            }
        });
        jButtonRun.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jButtonRunKeyTyped(evt);
            }
        });

        jButtonHelp.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jButtonHelp.setText("Help");
        jButtonHelp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonHelpMouseClicked(evt);
            }
        });
        jButtonHelp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jButtonHelpKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonSettings)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBoxAdjustTime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonOpenFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonRun))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonHelp)
                        .addGap(45, 45, 45)))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButtonHelp)
                        .addGap(29, 29, 29)))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSettings)
                    .addComponent(jCheckBoxAdjustTime)
                    .addComponent(jButtonRun)
                    .addComponent(jButtonOpenFile))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButtonSercel428ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonSercel428ActionPerformed
	sercel  = SercelType.SERCEL_428;

//	jRadioButtonSercel428.setSelected( true );
    }//GEN-LAST:event_jRadioButtonSercel428ActionPerformed

    private void jRadioButtonSercel408ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonSercel408ActionPerformed
	sercel  = SercelType.SERCEL_408;

//	jRadioButtonSercel408.setSelected( true );
    }//GEN-LAST:event_jRadioButtonSercel408ActionPerformed

    private void jRadioButtonSEGDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonSEGDActionPerformed
	input   = InputType.INPUT_SEGD;

//	jRadioButtonSEGD.setSelected( true );
    }//GEN-LAST:event_jRadioButtonSEGDActionPerformed

    private void jRadioButtonSercelRawActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonSercelRawActionPerformed
	input   = InputType.INPUT_RAW;

//	jRadioButtonSercelRaw.setSelected( true );
    }//GEN-LAST:event_jRadioButtonSercelRawActionPerformed

    private void jRadioButtonNavActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonNavActionPerformed
	input   = InputType.INPUT_NAV;

//	jRadioButtonNav.setSelected( true );
    }//GEN-LAST:event_jRadioButtonNavActionPerformed

    private void jCheckBoxAdjustTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxAdjustTimeActionPerformed
	bAdjustTime	= !bAdjustTime;

	jCheckBoxAdjustTime.setForeground( bAdjustTime ? Color.red : Color.black );
    }//GEN-LAST:event_jCheckBoxAdjustTimeActionPerformed

    private void jButtonSettingsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSettingsMouseClicked
	TBGenSettingsDialog sd	= new TBGenSettingsDialog( TBGenMainWindow.this, true );

	sd.fDynamiteOffset	= settings.fDynamiteOffset;
	sd.fVibratorOffset	= settings.fVibratorOffset;
	sd.fAirGunOffset	= settings.fAirGunOffset;
	sd.strProjectName	= settings.strProjectName;
	sd.bFirstNine		= settings.bFirstNine;

	TBGenSettingsDialog.DialogReturnType returnVal = sd.showDialog();

	if ( returnVal == TBGenSettingsDialog.DialogReturnType.OK_BUTTON )
	{
	    settings.fDynamiteOffset	= sd.fDynamiteOffset;
	    settings.fVibratorOffset	= sd.fVibratorOffset;
	    settings.fAirGunOffset	= sd.fAirGunOffset;
	    settings.strProjectName	= sd.strProjectName;
	    settings.bFirstNine		= sd.bFirstNine;

	    this.setTitle( "TBGen " + strVersion + ( ( settings.strProjectName.length() > 0 ) ? " [" + settings.strProjectName + "]" : "" ) );
	}
    }//GEN-LAST:event_jButtonSettingsMouseClicked

    private void jButtonOpenFileKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonOpenFileKeyTyped
	if ( evt.getKeyChar() == '\n' )
	{
	    jButtonOpenFileMouseClicked( null );
	}
    }//GEN-LAST:event_jButtonOpenFileKeyTyped

    private void jButtonSettingsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonSettingsKeyTyped
	if ( evt.getKeyChar() == '\n' )
	{
	    jButtonSettingsMouseClicked( null );
	}
    }//GEN-LAST:event_jButtonSettingsKeyTyped

    private void jButtonRunKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonRunKeyTyped
	if ( evt.getKeyChar() == '\n' )
	{
	    jButtonRunMouseClicked( null );
	}
    }//GEN-LAST:event_jButtonRunKeyTyped

    private void jButtonRunMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonRunMouseClicked

	int	nIndex;

	if ( files.length != 0 )
	{
	    if ( ( nIndex = files[ 0 ].getPath().lastIndexOf( File.separator ) ) != -1 )
	    {
		jTextArea1.setstrLogDir( files[ 0 ].getPath().substring( 0, nIndex + 1 ) );
		jTextArea1.setstrProjName( settings.strProjectName );
	    }
	}
	else
	{
	    if ( file.isDirectory() )
	    {
		jTextArea1.setstrLogDir( file.getPath() + File.separator );
		jTextArea1.setstrProjName( settings.strProjectName );
	    }
	}

	if ( file.isDirectory() )
	{
	    if ( files.length == 0 )
	    {
		//
		//	input parameter is a directory. fill in
		//	the files array with all the files in
		//	the direcory that match the desired
		//	extension.
		//

		File[] listOfFiles	= file.listFiles();
		List< File > fileList	= new ArrayList< File >();

		for ( File aFile : listOfFiles )
		{
		    if ( aFile.isFile() && ( aFile.getPath().endsWith( "segd" ) || aFile.getPath().endsWith( "SEGD" ) ) )
		    {
			fileList.add( aFile );
		    }
		}

		files	= fileList.toArray( new File[ fileList.size() ] );
	    }
	}

	//
	//	we now have a list of one or more files.
	//	depending on the file type, configure the
	//	progress bar based on the number of files (SEGD)
	//	or the number of lines in each file.
	//

	int nMax = 0;

	for ( File aFile : files )
	{
	    if ( input == InputType.INPUT_SEGD )
	    {
		nMax++;
	    }
	    else
	    {
		try
		{
		    nMax += CountLines( aFile.getPath() );
		}
		catch ( IOException e )
		{
		    System.err.println( "Error counting lines in file: " + e );
		}
	    }
	}

	jProgressBar1.setMinimum( 0 );
	jProgressBar1.setMaximum( nMax );
	jProgressBar1.setValue( 0 );

	//
	//	launch the thread that does the actual work.
	//

	FileReader fr = new FileReader( files,
					sercel,
					input,
					jProgressBar1,
					jTextArea1,
					bAdjustTime,
					settings );

	fr.execute();
    }//GEN-LAST:event_jButtonRunMouseClicked

    private void jButtonHelpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonHelpMouseClicked
	String path = TBGenMainWindow.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	int index   = path.toLowerCase().indexOf( "tbgen.jar" );

	if ( index != -1 )
	{
	   path	= path.substring( 0, index );
	}

	if ( path.lastIndexOf( '/' ) != path.length() - 1 )
	{
	    path    += "/";
	}

	try
	{
	    String decodedPath = URLDecoder.decode( path, "UTF-8" );
	    File pdfFile = new File( path + "TBGen_manual.pdf" );

	    if ( pdfFile.exists() )
	    {
		if ( Desktop.isDesktopSupported() )
		{
		    Desktop.getDesktop().open( pdfFile );
		}
		else
		{
		    System.out.println( "Awt Desktop is not supported!" );
		}
	    }
	    else
	    {
		jTextArea1.append( "File " + pdfFile.getPath() + " not found!\n" );
	    }
	}
	catch ( Exception ex )
	{
	    ex.printStackTrace();
	}
    }//GEN-LAST:event_jButtonHelpMouseClicked

    private void jButtonHelpKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonHelpKeyTyped
	if ( evt.getKeyChar() == '\n' )
	{
	    jButtonHelpMouseClicked( null );
	}
    }//GEN-LAST:event_jButtonHelpKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton jButtonHelp;
    private javax.swing.JButton jButtonOpenFile;
    private javax.swing.JButton jButtonRun;
    private javax.swing.JButton jButtonSettings;
    private javax.swing.JCheckBox jCheckBoxAdjustTime;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JRadioButton jRadioButtonNav;
    private javax.swing.JRadioButton jRadioButtonSEGD;
    private javax.swing.JRadioButton jRadioButtonSercel408;
    private javax.swing.JRadioButton jRadioButtonSercel428;
    private javax.swing.JRadioButton jRadioButtonSercelRaw;
    private javax.swing.JScrollPane jScrollPane1;
    private MyTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}

//
//	JFileFilter
//
//	extends the FileFilter class to allow filtering of files in file open
//	dialog box.
//

class JFileFilter extends javax.swing.filechooser.FileFilter
{
	protected String description;
	protected ArrayList exts = new ArrayList();

	public void addType( String s )
	{
	    exts.add( s );
	}

	//
	//	Return true if the given file is accepted by this filter.
	//

	public boolean accept( File f )
	{
	    //
	    //	    Little trick: if you don't do this, only directory names
	    //	    ending in one of the extentions appear in the window.
	    //

	    if ( f.isDirectory() )
	    {
		return true;
	    }
	    else if ( f.isFile() )
	    {
		Iterator it = exts.iterator();

		while ( it.hasNext() )
		{
		    if ( f.getName().endsWith( ( String ) it.next() ) )
			return true;
		}
	    }

	    //
	    //	    A file that didn't match, or a weirdo (e.g. UNIX device
	    //	    file?).
	    //

	    return false;
	}

	//
	//	Set the printable description of this filter.
	//

	public void setDescription( String s )
	{
	    description = s;
	}

	//
	//	Return the printable description of this filter.
	//

	public String getDescription()
	{
	    return description;
	}
}

//
//	end of TBGenMainWindow.java.
//
