package com.ggs.tools;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ggs.tools.tbgen.TBGen;

/**
 * Controller for AutoSeis ToolKit main screen with launch buttons
 * 
 * @author Oliver.Hsu
 *
 */
public class ToolKitController implements Initializable {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToolKitController.class);

	@FXML
	private Label versionLabel;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		String version = null; 
		String buildDate = null; 
		Properties properties = new Properties();
		try {
			properties.load(ToolKitController.class.getResourceAsStream("/tools.properties"));
			version = properties.getProperty("jenkins.build.number");
			buildDate = properties.getProperty("maven.build.date");
		} catch (IOException e) {
			LOGGER.error("Error reading version from properties file");
		}		
		versionLabel.setText("Build Version : " +version + "  Date :" + buildDate);		
	}

	/**
	 * Launches TBGen
	 */
	public void handleTBGen() {
		LOGGER.info("Starting TBGen");
		TBGen.main(new String[0]);
	}

	/**
	 * Launches Passive Data Tool
	 */
	public void handlePassiveData() {
		LOGGER.info("Starting Passive Data");
		try {
			Stage stage = new Stage();
			stage.setTitle("Passive Data Time Break Generation");
			Parent parent = FXMLLoader.load(getClass().getResource("/fxml/PassiveData.fxml"));
			stage.setScene(new Scene(parent));
			stage.show();
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(), ioe);
		}

	}

	/**
	 * Launches Passive Data Tool
	 */
	public void handleTestIfI() {
		LOGGER.info("Starting Verif-i");
		try {
			Stage stage = new Stage();
			stage.setTitle("Verif-i Time Break Generation");
			Parent parent = FXMLLoader.load(getClass().getResource("/fxml/TestIfI.fxml"));
			stage.setScene(new Scene(parent));
			stage.show();
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(), ioe);
		}

	}

	/**
	 * Loads the GPS conversion screen
	 * 
	 * @throws IOException
	 */
	public void handleTimeConversionBtn() throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/TimeConverter.fxml"));
		Scene scene = new Scene(root);
		Stage stage = new Stage();
		stage.setTitle("Time Converter");
		stage.setScene(scene);
		stage.show();
	}
}
