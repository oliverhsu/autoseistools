package com.ggs.tools.passivedata;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.controlsfx.dialog.Dialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;

import com.ggs.tools.util.CsvFileUtil;
import com.ggs.tools.util.FileDialogs;

/**
 * FXML controller for Passive Data
 * @author Oliver.Hsu
 *
 */
public class TestIfIController implements Initializable {

	@FXML
	private TextField lineTextField;
	
	@FXML
	private TextField stationTextField;

	@FXML
	private TextField ffidTextField;

	@FXML
	private TextField ffidIncField;
	
	@FXML
	private TextField extractDurationMillisec;

	@FXML
	private TextField traceLengthHour;

	@FXML
	private TextField traceLengthMinute;

	@FXML
	private TextField traceLengthSec;

	@FXML
	private TextField traceLengthMillisec;
	
	@FXML
	private TextField testIfIFileTextField;
	
	@FXML
	private CheckBox timeAdjustmentCheckBox;
	
	@FXML
	private ChoiceBox<Double> sampleRateChoiceBox;
	
	@FXML
	private Button generateButton;
	
	@FXML
	private TableView<CsvData> previewTable;
		
	@FXML
	private TableColumn<CsvData, Double> lineNumColumn;
	
	@FXML
	private TableColumn<CsvData, Double> stationNumColumn;
	
	@FXML
	private TableColumn<CsvData, LocalDateTime> shotTimeColumn;

	@FXML
	private TableColumn<CsvData, Long> gpsTimeColumn;

	@FXML
	private TableColumn<CsvData, Long> aquisitionLengthColumn;
	
	@FXML
	private TableColumn<CsvData, Integer> shotIdColumn;
	
	@FXML
	private TableColumn<CsvData, Integer> pointIndexColumn;

	@FXML
	private TableColumn<CsvData, Integer> upholeTimeColumn;

	@FXML
	private TableColumn<CsvData, String> sourceTypeColumn;

	private Double lineNum;
	private Double stationNum;
	private Integer ffid;
	private Integer ffidIncrement;
	private long traceLengthMilli = 0;
	private File testIfFile;
	
	private static final int SECS_IN_HOUR = 60 * 60;
	private static final int SECS_IN_MINUTE = 60;
	private static final int MILLISECS_IN_SEC = 1000;
	private static final int MAX_SAMPLES = 65535;
	private static final DecimalFormat df2 = new DecimalFormat("#.##");
	
	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
	private final DateTimeFormatter dateTimeJulianFormat = DateTimeFormatter.ofPattern("DDDHHmmss");
	
	private ExecutorService executorService = Executors.newSingleThreadExecutor();
	
	TestIfIDataGenerator testIfIDataGenerator = new TestIfIDataGenerator();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestIfIController.class);
	
	/**
	 * Called by the Generate button
	 */
	public void handleGenerateButton() {
		
		boolean validData = validate();
		
		if (validData) {
			// prompt for export file
			List<ExtensionFilter> extension = new ArrayList<ExtensionFilter>();
			extension.add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
			final String fileName = "testIfITimeBreak-" + LocalDateTime.now().format(DATE_TIME_FORMAT) + ".csv";
			
			File csvOutputFile = FileDialogs.saveFileChooser("Choose where to store CSV file", new File(System.getProperty("user.home")),
					null, extension, fileName);

			if (csvOutputFile != null) {
				startExportThread(csvOutputFile);	
			}
			
		}

		
	}
	
	/**
	 * Exports Passive data shots file in background thread
	 * @param file
	 */
	private void startExportThread(File file) {
		final Task<Void> exportTask = new Task<Void>() {
			@Override protected Void call() throws Exception {
				
				List<CsvData> shotData = generatePassiveData();								
				
				CsvFileUtil.writeCsvFile(file.toPath(), shotData);
				
				Platform.runLater(() -> {
					ObservableList<CsvData> previewTableData = FXCollections.observableArrayList();
					previewTableData.addAll(shotData);
					previewTable.setItems(previewTableData);
				});
				
				
				return null;
			}

		};
		
		
		EventHandler<WorkerStateEvent> taskEvent = new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent t) {

					 
					if (t.getEventType() == WorkerStateEvent.WORKER_STATE_CANCELLED) {
						Dialogs.create().owner(getParentStage()).message(exportTask.getException().getMessage()).masthead("Error Generating Time Break File").title("Passive Data Generation").showError();
						
					} else if (t.getEventType() == WorkerStateEvent.WORKER_STATE_FAILED) {
						Throwable ex = exportTask.getException();
						LOGGER.error(ex.getMessage(), ex);
						Dialogs.create().owner(getParentStage()).message(exportTask.getException().getMessage()).masthead("Error Generating Time Break File").title("Passive Data Generation").showException(ex);
						
					} else if (t.getEventType() == WorkerStateEvent.WORKER_STATE_SUCCEEDED) {
						Dialogs.create().owner(getParentStage()).message("Time breaks file saved to " + file.getAbsolutePath()).masthead("Time Break File Generated").title("Passive Data Generation").showInformation();;				
					} else {
						Dialogs.create().owner(getParentStage()).message(t.getEventType().getName()).masthead("Error Generating Time Break File").title("Passive Data Generation").showError();
					}
					

			}
		};
		exportTask.setOnSucceeded(taskEvent);
		exportTask.setOnCancelled(taskEvent);
		exportTask.setOnFailed(taskEvent);
		
		executorService.submit(exportTask);
		
	}

	/**
	 * Exports Passive data shots file in background thread
	 * @param file
	 */
	private void startPreviewThread() {
		final Task<Void> exportTask = new Task<Void>() {
			@Override protected Void call() throws Exception {
				
				List<CsvData> shotData = generatePassiveData();
				
				Platform.runLater(() -> {
					ObservableList<CsvData> previewTableData = FXCollections.observableArrayList();
					previewTableData.addAll(shotData);
					previewTable.setItems(previewTableData);
				}
		);
				return null;
			}

		};
		
		EventHandler<WorkerStateEvent> taskEvent = new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent t) {

					 
					if (t.getEventType() == WorkerStateEvent.WORKER_STATE_CANCELLED) {
						Dialogs.create().owner(getParentStage()).message(exportTask.getException().getMessage()).masthead("Error Generating Time Break File").title("Test-If-I Data Generation").showError();
						
					} else if (t.getEventType() == WorkerStateEvent.WORKER_STATE_FAILED) {
						Throwable ex = exportTask.getException();
						LOGGER.error(ex.getMessage(), ex);
						Dialogs.create().owner(getParentStage()).message(exportTask.getException().getMessage()).masthead("Error Generating Time Break File").title("Test-If-I Data Generation").showException(ex);
						
					} else {
						Dialogs.create().owner(getParentStage()).message(t.getEventType().getName()).masthead("Error Generating Time Break File").title("Test-If-I Data Generation").showError();
					}
					

			}
		};

		exportTask.setOnCancelled(taskEvent);
		exportTask.setOnFailed(taskEvent);
		
		
		
		executorService.submit(exportTask);
		
	}
	
	/**
	 * Called by the Preview button
	 */
	public void handlePreviewButton() {
		
		boolean validData = validate();
		
		if (validData) {
			startPreviewThread();
		}
		
	}
	
	/**
	 * Called by the Test-If-I Select File button 
	 */
	@FXML
	public void handleSelectFileButton() {

		testIfFile = FileDialogs.showFileChooser("Select Import File", new File(System.getProperty("user.home")),getParentStage());

			if (testIfFile != null && testIfFile.exists()) {
				testIfIFileTextField.setText(testIfFile.getAbsolutePath());
				LOGGER.debug("{} test-if-i file is selected",testIfFile.getAbsolutePath());
			}
	}
	
	
	/**
	 * Generates Passive Data using the inputs in the controller and calling PassiveDataGenerator
	 * @return List<CsvData> of passive data shots
	 */
	private List<CsvData> generatePassiveData() throws IOException {
		
		PassiveDataInput passiveDataInput = new PassiveDataInput();
		passiveDataInput.setFfid(ffid);
		passiveDataInput.setFfidIncrement(ffidIncrement);
		passiveDataInput.setLineNum(lineNum);
		passiveDataInput.setStationNum(stationNum);
		passiveDataInput.setTraceLengthMilli(traceLengthMilli);

		
		List<CsvData> shotData = testIfIDataGenerator.testIfIDataGenerator(passiveDataInput, testIfFile.toPath(), timeAdjustmentCheckBox.isSelected());
		return shotData;
	}
	
	
	private boolean validate() {
		
		List<String> errorMessages = new ArrayList<>();
		
		// validate line and station
		if (lineTextField.getText().isEmpty() || stationTextField.getText().isEmpty()) {
			errorMessages.add("Line and Station must be entered");
		}
		else {
			try {
				lineNum = Double.valueOf(lineTextField.getText());
				stationNum = Double.valueOf(stationTextField.getText());
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("Line and Station Number must be numeric");
			}
		}

		// validate ffid
		if (ffidTextField.getText().isEmpty() || ffidIncField.getText().isEmpty()) {
			errorMessages.add("FFID and FFID Increment must be entered");
		}
		else {
			try {
				ffid = Integer.valueOf(ffidTextField.getText());
				ffidIncrement = Integer.valueOf(ffidIncField.getText());
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("Line and Station Number must be numeric");
			}
		}
		

		// validate trace length
		if (traceLengthHour.getText().isEmpty() || traceLengthMinute.getText().isEmpty()
				|| traceLengthSec.getText().isEmpty() || traceLengthMillisec.getText().isEmpty()) {
			errorMessages.add("Trace length must be specified");
		}
		else {
			// convert hours, minutes, secs, millisecs to a sum millisecs
			try {
				traceLengthMilli = (Long.parseLong(traceLengthHour.getText()) * SECS_IN_HOUR
				+ Long.parseLong(traceLengthMinute.getText()) * SECS_IN_MINUTE + Long.parseLong(traceLengthSec.getText())) * MILLISECS_IN_SEC;
				
				traceLengthMilli = traceLengthMilli + Long.parseLong(traceLengthMillisec.getText());
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("Trace length must be numeric");
			}
		}
		
		// validate testifi file
		if (testIfFile == null) {
			errorMessages.add("Valid Test-If-I file must be choosen");
		}
				

		// create error dialog for validation failures
		if (errorMessages.size() > 0) {
			Dialogs.create().owner(getParentStage()).message(errorMessages.toString()).masthead("Invalid Entries").title("Passive Data Generation").showError();	
		}
		
		
		// create warning dialog
		// validate trace length is less than max samples
		if (traceLengthMilli / sampleRateChoiceBox.getValue() > MAX_SAMPLES) {
			Dialogs.create().owner(getParentStage()).message("The amount of data in the trace length may not be supported by all processing systems").masthead("Invalid Entries").title("Passive Data Generation").showWarning();
		}
		

		
		
		return errorMessages.size() == 0 ? true : false; 
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		

		// populate sample rate choice box
		sampleRateChoiceBox.getItems().add(1d);
		sampleRateChoiceBox.getItems().add(2d);
		sampleRateChoiceBox.setValue(2d);

		// init preview table
		//Assign value to Columns 
		lineNumColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Double>("lineNum"));

		stationNumColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Double>("stationNum"));;

		shotTimeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, LocalDateTime>("shotTime"));;

		gpsTimeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Long>("gpsTime"));;

		shotIdColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Integer>("shotId"));;

		aquisitionLengthColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Long>("aquisitionLength"));;

		pointIndexColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Integer>("pointIndex"));;

		upholeTimeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Integer>("upholeTime"));;

		sourceTypeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, String>("sourceType"));;

		lineNumColumn.setCellFactory(new Callback<TableColumn<CsvData, Double>, TableCell<CsvData, Double>>() {
		    @Override
		    public TableCell<CsvData, Double> call(TableColumn<CsvData, Double> param) {
		        return new TableCell<CsvData, Double>() {
		            @Override
		            protected void updateItem(Double item, boolean empty) {
		            	super.updateItem(item, empty);
		            	if (!empty) {		                
		            		setText(df2.format(item));
			            } else {
			            	setText(null);
			            }
		            }
		        };
		    }
		});

		stationNumColumn.setCellFactory(new Callback<TableColumn<CsvData, Double>, TableCell<CsvData, Double>>() {
		    @Override
		    public TableCell<CsvData, Double> call(TableColumn<CsvData, Double> param) {
		        return new TableCell<CsvData, Double>() {
		            @Override
		            protected void updateItem(Double item, boolean empty) {
		            	super.updateItem(item, empty);
		            	if (!empty) {		                
		            		setText(df2.format(item));
			            } else {
			            	setText(null);
			            }
		            }
		        };
		    }
		});
		
		shotTimeColumn.setCellFactory(new Callback<TableColumn<CsvData, LocalDateTime>, TableCell<CsvData, LocalDateTime>>() {
		    @Override
		    public TableCell<CsvData, LocalDateTime> call(TableColumn<CsvData, LocalDateTime> param) {
		        return new TableCell<CsvData, LocalDateTime>() {
		            @Override
		            protected void updateItem(LocalDateTime item, boolean empty) {
		            	super.updateItem(item, empty);
		            	if (!empty) {		                
		            		setText(item.format(dateTimeJulianFormat));
		            	} else {
		            		setText(null);
		            	}
		            }
		        };
		    }
		});
		
		
	}
	
	private Stage getParentStage() {
		return (Stage) generateButton.getScene().getWindow();
	}
}
