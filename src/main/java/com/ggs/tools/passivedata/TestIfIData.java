package com.ggs.tools.passivedata;

import java.time.LocalDate;
import java.time.LocalTime;

public class TestIfIData {
	
	private int eventTimeMicroSecs;
	private LocalTime eventTime;
	private LocalDate eventDate;
	
	public TestIfIData(int eventTimeMicroSecs, LocalTime eventTime,	LocalDate eventDate) {
		super();
		this.eventTimeMicroSecs = eventTimeMicroSecs;
		this.eventTime = eventTime;
		this.eventDate = eventDate;
	}

	public int getEventTimeMicroSecs() {
		return eventTimeMicroSecs;
	}

	public void setEventTimeMicroSecs(int eventTimeMicroSecs) {
		this.eventTimeMicroSecs = eventTimeMicroSecs;
	}

	public LocalTime getEventTime() {
		return eventTime;
	}

	public void setEventTime(LocalTime eventTime) {
		this.eventTime = eventTime;
	}

	public LocalDate getEventDate() {
		return eventDate;
	}

	public void setEventDate(LocalDate eventDate) {
		this.eventDate = eventDate;
	}

	@Override
	public String toString() {
		return "TestIfIData [eventTimeMicroSecs=" + eventTimeMicroSecs
				+ ", eventTime=" + eventTime + ", eventDate=" + eventDate + "]";
	}
	
	
	

}
