package com.ggs.tools.passivedata;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.controlsfx.dialog.Dialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ListCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;

import com.ggs.tools.util.CsvFileUtil;
import com.ggs.tools.util.FileDialogs;

/**
 * FXML controller for Passive Data
 * @author Oliver.Hsu
 *
 */
public class PassiveDataController implements Initializable {

	@FXML
	private TextField lineTextField;
	
	@FXML
	private TextField stationTextField;

	@FXML
	private TextField ffidTextField;

	@FXML
	private TextField ffidIncField;
	
	@FXML
	private DatePicker startDatePicker;

	@FXML
	private DatePicker endDatePicker;

	@FXML
	private TextField intervalTextField;
	
	@FXML
	private CheckBox alternateScheduleCheckbox;

	@FXML
	private CheckBox resetFfidCheckbox;

	@FXML
	private ComboBox<String> startTimeComboBox;

	@FXML
	private ComboBox<ZoneId> timeZoneComboBox;
	
	@FXML
	private TextField extractDurationDay;

	@FXML
	private TextField extractDurationHour;

	@FXML
	private TextField extractDurationMinute;

	@FXML
	private TextField extractDurationSec;

	@FXML
	private TextField extractDurationMillisec;

	@FXML
	private TextField traceLengthHour;

	@FXML
	private TextField traceLengthMinute;

	@FXML
	private TextField traceLengthSec;

	@FXML
	private TextField traceLengthMillisec;
	
	@FXML
	private ChoiceBox<Double> sampleRateChoiceBox;
	
	@FXML
	private Button generateButton;
	
	@FXML
	private TableView<CsvData> previewTable;
		
	@FXML
	private TableColumn<CsvData, Double> lineNumColumn;
	
	@FXML
	private TableColumn<CsvData, Double> stationNumColumn;
	
	@FXML
	private TableColumn<CsvData, LocalDateTime> shotTimeColumn;

	@FXML
	private TableColumn<CsvData, Long> gpsTimeColumn;

	@FXML
	private TableColumn<CsvData, Long> aquisitionLengthColumn;
	
	@FXML
	private TableColumn<CsvData, Integer> shotIdColumn;
	
	@FXML
	private TableColumn<CsvData, Integer> pointIndexColumn;

	@FXML
	private TableColumn<CsvData, Integer> upholeTimeColumn;

	@FXML
	private TableColumn<CsvData, String> sourceTypeColumn;

	private Double lineNum;
	private Double stationNum;
	private Integer ffid;
	private Integer ffidIncrement;
	private LocalDate startDate;
	private LocalDate endDate;
	private Integer intervalDays;
	private LocalTime startTime;
	private long extractDurationMilli = 0;
	private long traceLengthMilli = 0;
	
	
	private final LocalDateTime localDateTime = LocalDateTime.now();;
	private final ZoneOffset utcOffset = ZoneOffset.of("Z");
	private static final int SECS_IN_DAY = 60 * 60 * 24;
	private static final int SECS_IN_HOUR = 60 * 60;
	private static final int SECS_IN_MINUTE = 60;
	private static final int MILLISECS_IN_SEC = 1000;
	private static final int MAX_SAMPLES = 65535;
	private static final DecimalFormat df2 = new DecimalFormat("#.##");
	
	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
	private final DateTimeFormatter dateTimeJulianFormat = DateTimeFormatter.ofPattern("DDDHHmmss");
	
	private ExecutorService executorService = Executors.newSingleThreadExecutor();
	
	PassiveDataGenerator passiveDataGenerator = new PassiveDataGenerator();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PassiveDataController.class);
	
	/**
	 * Called by the Generate button
	 */
	public void handleGenerateButton() {
		
		boolean validData = validate();
		
		if (validData) {
			// prompt for export file
			List<ExtensionFilter> extension = new ArrayList<ExtensionFilter>();
			extension.add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
			final String fileName = "passiveDataTimeBreak-" + LocalDateTime.now().format(DATE_TIME_FORMAT) + ".csv";
			
			File csvOutputFile = FileDialogs.saveFileChooser("Choose where to store CSV file", new File(System.getProperty("user.home")),
					null, extension, fileName);

			if (csvOutputFile != null) {
				startExportThread(csvOutputFile);	
			}
			
		}

		
	}
	
	/**
	 * Exports Passive data shots file in background thread
	 * @param file
	 */
	private void startExportThread(File file) {
		final Task<Void> exportTask = new Task<Void>() {
			@Override protected Void call() throws Exception {
				
				List<CsvData> shotData = generatePassiveData();
				
				CsvFileUtil.writeCsvFile(file.toPath(), shotData);
				
				Platform.runLater(() -> {
					ObservableList<CsvData> previewTableData = FXCollections.observableArrayList();
					previewTableData.addAll(shotData);
					previewTable.setItems(previewTableData);
				}
			);				
				
				return null;
			}

		};
		
		
		EventHandler<WorkerStateEvent> taskEvent = new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent t) {

					 
					if (t.getEventType() == WorkerStateEvent.WORKER_STATE_CANCELLED) {
						Dialogs.create().owner(getParentStage()).message(exportTask.getException().getMessage()).masthead("Error Generating Time Break File").title("Passive Data Generation").showError();
						
					} else if (t.getEventType() == WorkerStateEvent.WORKER_STATE_FAILED) {
						Throwable ex = exportTask.getException();
						LOGGER.error(ex.getMessage(), ex);
						Dialogs.create().owner(getParentStage()).message(exportTask.getException().getMessage()).masthead("Error Generating Time Break File").title("Passive Data Generation").showException(ex);
						
					} else if (t.getEventType() == WorkerStateEvent.WORKER_STATE_SUCCEEDED) {
						Dialogs.create().owner(getParentStage()).message("Time breaks file saved to " + file.getAbsolutePath()).masthead("Time Break File Generated").title("Passive Data Generation").showInformation();;				
					} else {
						Dialogs.create().owner(getParentStage()).message(t.getEventType().getName()).masthead("Error Generating Time Break File").title("Passive Data Generation").showError();
					}
					

			}
		};
		exportTask.setOnSucceeded(taskEvent);
		exportTask.setOnCancelled(taskEvent);
		exportTask.setOnFailed(taskEvent);
		
		executorService.submit(exportTask);
		
	}

	/**
	 * Exports Passive data shots file in background thread
	 * @param file
	 */
	private void startPreviewThread() {
		final Task<Void> exportTask = new Task<Void>() {
			@Override protected Void call() throws Exception {
				
				List<CsvData> shotData = generatePassiveData();
				
				Platform.runLater(() -> {
					ObservableList<CsvData> previewTableData = FXCollections.observableArrayList();
					previewTableData.addAll(shotData);
					previewTable.setItems(previewTableData);
				}
		);
				return null;
			}

		};
		
		
		executorService.submit(exportTask);
		
	}
	

	public void handlePreviewButton() {
		
		boolean validData = validate();
		
		if (validData) {
			startPreviewThread();
		}
		
	}
	
	/**
	 * Generates Passive Data using the inputs in the controller and calling PassiveDataGenerator
	 * @return List<CsvData> of passive data shots
	 */
	private List<CsvData> generatePassiveData() {
		
		PassiveDataInput passiveDataInput = new PassiveDataInput();
		passiveDataInput.setStartDate(startDate);
		passiveDataInput.setEndDate(endDate);
		passiveDataInput.setExtractDurationMilli(extractDurationMilli);
		passiveDataInput.setFfid(ffid);
		passiveDataInput.setFfidIncrement(ffidIncrement);
		passiveDataInput.setIntervalDays(intervalDays);
		passiveDataInput.setLineNum(lineNum);
		passiveDataInput.setStationNum(stationNum);
		passiveDataInput.setResetFfid(resetFfidCheckbox.isSelected());
		passiveDataInput.setStartTime(startTime);
		passiveDataInput.setTraceLengthMilli(traceLengthMilli);
		passiveDataInput.setZoneId(timeZoneComboBox.getValue());

		
		List<CsvData> shotData = passiveDataGenerator.generatePassiveData(passiveDataInput);
		return shotData;
	}
	
	
	private boolean validate() {
		
		List<String> errorMessages = new ArrayList<>();
		
		// validate line and station
		if (lineTextField.getText().isEmpty() || stationTextField.getText().isEmpty()) {
			errorMessages.add("Line and Station must be entered");
		}
		else {
			try {
				lineNum = Double.valueOf(lineTextField.getText());
				stationNum = Double.valueOf(stationTextField.getText());
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("Line and Station Number must be numeric");
			}
		}

		// validate ffid
		if (ffidTextField.getText().isEmpty() || ffidIncField.getText().isEmpty()) {
			errorMessages.add("FFID and FFID Increment must be entered");
		}
		else {
			try {
				ffid = Integer.valueOf(ffidTextField.getText());
				ffidIncrement = Integer.valueOf(ffidIncField.getText());
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("Line and Station Number must be numeric");
			}
		}
		
		// validate start and end date
		if (startDatePicker.getValue() == null || endDatePicker.getValue() == null) {
			errorMessages.add("Choose start and end date");
		}
		else {
			startDate = startDatePicker.getValue();
			endDate = endDatePicker.getValue();
			
			// check alternate schedule
			if (alternateScheduleCheckbox.isSelected()) {
				LocalDate alternateStartDate = passiveDataGenerator.alternateScheduleStartDate(startDate, intervalDays);
				if (!alternateStartDate.equals(startDate)) {
					Dialogs.create().owner(getParentStage()).message("Alternate start date starts on " + alternateStartDate).masthead("Alternate Start Date").title("Passive Data Generation").showWarning();
					startDatePicker.setValue(alternateStartDate);
					startDate = alternateStartDate;
				}
			}
						
		}
		
		// verify end date is after start date
		if (!endDate.isAfter(startDate)) {
			errorMessages.add("End date must be after start date");
		}

		
		// validate interval days
		if (intervalTextField.getText().isEmpty()) {
			errorMessages.add("Interval must be specified");
		}
		else {
			try {
				intervalDays = Integer.valueOf(intervalTextField.getText());	
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("Interval must be numeric");
			}

			
		}
		
		// validate start time
		if (startTimeComboBox.getValue() == null || startTimeComboBox.getValue().isEmpty()) {
			errorMessages.add("Start time must be selected");
		}
		else {
			try {
				startTime = LocalTime.parse(startTimeComboBox.getValue());	
			}
			catch (DateTimeParseException dtpe) {
				errorMessages.add("Time must be in HH:MM 24 hour format");
			}
			
		}
		
		// validate extraction duration
		if (extractDurationDay.getText().isEmpty() || extractDurationHour.getText().isEmpty() || extractDurationMinute.getText().isEmpty()
				|| extractDurationSec.getText().isEmpty() || extractDurationMillisec.getText().isEmpty()) {
			errorMessages.add("Extract duration must be specified");
		}
		else {
			// convert days, hours, minutes, secs, millisecs to a sum millisecs
			try {
				extractDurationMilli = (Long.parseLong(extractDurationDay.getText()) * SECS_IN_DAY + Long.parseLong(extractDurationHour.getText()) * SECS_IN_HOUR
				+ Long.parseLong(extractDurationMinute.getText()) * SECS_IN_MINUTE + Long.parseLong(extractDurationSec.getText())) * MILLISECS_IN_SEC;
				
				extractDurationMilli = extractDurationMilli + Long.parseLong(extractDurationMillisec.getText());
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("The extraction duration must be numeric");
			}
		}

		// validate trace length
		if (traceLengthHour.getText().isEmpty() || traceLengthMinute.getText().isEmpty()
				|| traceLengthSec.getText().isEmpty() || traceLengthMillisec.getText().isEmpty()) {
			errorMessages.add("Trace length must be specified");
		}
		else {
			// convert hours, minutes, secs, millisecs to a sum millisecs
			try {
				traceLengthMilli = (Long.parseLong(traceLengthHour.getText()) * SECS_IN_HOUR
				+ Long.parseLong(traceLengthMinute.getText()) * SECS_IN_MINUTE + Long.parseLong(traceLengthSec.getText())) * MILLISECS_IN_SEC;
				
				traceLengthMilli = traceLengthMilli + Long.parseLong(traceLengthMillisec.getText());
			}
			catch (NumberFormatException nfe) {
				errorMessages.add("Trace length must be numeric");
			}
		}
		
		// validate trace length is less than the duration
		if (traceLengthMilli > extractDurationMilli) {
			errorMessages.add("The trace length must be less than the extraction duration");
		}
		

		// create error dialog for validation failures
		if (errorMessages.size() > 0) {
			Dialogs.create().owner(getParentStage()).message(errorMessages.toString()).masthead("Invalid Entries").title("Passive Data Generation").showError();	
		}
		
		
		// create warning dialog
		// validate trace length is less than max samples
		if (traceLengthMilli / sampleRateChoiceBox.getValue() > MAX_SAMPLES) {
			Dialogs.create().owner(getParentStage()).message("The amount of data in the trace length may not be supported by all processing systems").masthead("Trace length warning").title("Passive Data Generation").showWarning();
		}
		

		
		
		return errorMessages.size() == 0 ? true : false; 
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		// Initialize start time combo box with times from every 30 minutes from 00:00 to 23:30
		for (int hour = 00; hour < 24; hour++) {
			startTimeComboBox.getItems().add(String.format("%02d", hour).concat(":00"));
			startTimeComboBox.getItems().add(String.format("%02d", hour).concat(":30"));
		}
		
		// set to editable so they can enter a custom time
		startTimeComboBox.setEditable(true);
		
		// Initialize time zone combo box
		// Create a List using the set of zones and sort it.
		Set<String> allZones = ZoneId.getAvailableZoneIds();
		List<String> zoneList = new ArrayList<String>(allZones);
		Collections.sort(zoneList);
		
		for (String s : zoneList) {
		    ZoneId zone = ZoneId.of(s);
		    timeZoneComboBox.getItems().add(zone);

		}
		
		timeZoneComboBox.setValue(ZoneId.systemDefault());

		// set custom renderer for combobox to show time zone with utc offset
		timeZoneComboBox.setCellFactory(new Callback<ListView<ZoneId>, ListCell<ZoneId>>() {

            @Override
            public ListCell<ZoneId> call(ListView<ZoneId> arg0) {
                return new ListCell<ZoneId>() {

                    @Override
                    protected void updateItem(ZoneId zone, boolean empty) {
                        super.updateItem(zone, empty);

                        if (zone != null || !empty) {
                		    ZonedDateTime zdt = localDateTime.atZone(zone);
                		    ZoneOffset offset = zdt.getOffset();
                		    String zoneString = String.format("%s %s\n", zone, offset.equals(utcOffset) ? "UTC" : offset);
                            setText(zoneString);
                            
                        }
                    }
                };
            }
        });

		// populate sample rate choice box
		sampleRateChoiceBox.getItems().add(1d);
		sampleRateChoiceBox.getItems().add(2d);
		sampleRateChoiceBox.setValue(2d);

		// init preview table
		//Assign value to Columns 
		lineNumColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Double>("lineNum"));

		stationNumColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Double>("stationNum"));;

		shotTimeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, LocalDateTime>("shotTime"));;

		gpsTimeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Long>("gpsTime"));;

		shotIdColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Integer>("shotId"));;

		aquisitionLengthColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Long>("aquisitionLength"));;

		pointIndexColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Integer>("pointIndex"));;

		upholeTimeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, Integer>("upholeTime"));;

		sourceTypeColumn.setCellValueFactory(new PropertyValueFactory<CsvData, String>("sourceType"));;

		lineNumColumn.setCellFactory(new Callback<TableColumn<CsvData, Double>, TableCell<CsvData, Double>>() {
		    @Override
		    public TableCell<CsvData, Double> call(TableColumn<CsvData, Double> param) {
		        return new TableCell<CsvData, Double>() {
		            @Override
		            protected void updateItem(Double item, boolean empty) {
		            	super.updateItem(item, empty);
		            	if (!empty) {		                
		            		setText(df2.format(item));
			            } else {
			            	setText(null);
			            }
		            }
		        };
		    }
		});

		stationNumColumn.setCellFactory(new Callback<TableColumn<CsvData, Double>, TableCell<CsvData, Double>>() {
		    @Override
		    public TableCell<CsvData, Double> call(TableColumn<CsvData, Double> param) {
		        return new TableCell<CsvData, Double>() {
		            @Override
		            protected void updateItem(Double item, boolean empty) {
		            	super.updateItem(item, empty);
		            	if (!empty) {		                
		            		setText(df2.format(item));
			            } else {
			            	setText(null);
			            }
		            }
		        };
		    }
		});
		
		shotTimeColumn.setCellFactory(new Callback<TableColumn<CsvData, LocalDateTime>, TableCell<CsvData, LocalDateTime>>() {
		    @Override
		    public TableCell<CsvData, LocalDateTime> call(TableColumn<CsvData, LocalDateTime> param) {
		        return new TableCell<CsvData, LocalDateTime>() {
		            @Override
		            protected void updateItem(LocalDateTime item, boolean empty) {
		            	super.updateItem(item, empty);
		            	if (!empty) {		                
		            		setText(item.format(dateTimeJulianFormat));
		            	} else {
		            		setText(null);
		            	}
		            }
		        };
		    }
		});
		
		
	}
	
	private Stage getParentStage() {
		return (Stage) generateButton.getScene().getWindow();
	}
}
