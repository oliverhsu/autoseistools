package com.ggs.tools.passivedata;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import com.ggs.tools.util.CsvWriteable;

/**
 * Bean for Csv Data
 * @author Oliver.Hsu
 *
 */
public class CsvData implements CsvWriteable {
	
	private Double lineNum;
	private Double stationNum;
	private LocalDateTime shotTime;
	private Long gpsTime;
	private Integer shotId;
	private Long aquisitionLength;
	private Integer pointIndex;
	private Integer upholeTime;
	private String sourceType;
	
	private static final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("DDDHHmmss");
	
	public CsvData(CsvDataBuilder builder)  {
		
		this.lineNum = builder.getLineNum();
		this.stationNum = builder.getStationNum();
		this.shotTime = builder.getShotTime();
		this.gpsTime = builder.getGpsTime();
		this.shotId = builder.getShotId();
		this.aquisitionLength = builder.getAquisitionLength();
		this.pointIndex = builder.getPointIndex();
		this.upholeTime = builder.getUpholeTime();
		this.sourceType = builder.getSourceType();
	}
	
	
	
	public Double getLineNum() {
		return lineNum;
	}



	public Double getStationNum() {
		return stationNum;
	}



	public LocalDateTime getShotTime() {
		return shotTime;
	}



	public Long getGpsTime() {
		return gpsTime;
	}



	public Integer getShotId() {
		return shotId;
	}



	public Long getAquisitionLength() {
		return aquisitionLength;
	}



	public Integer getPointIndex() {
		return pointIndex;
	}



	public Integer getUpholeTime() {
		return upholeTime;
	}



	public String getSourceType() {
		return sourceType;
	}
	

	@Override
	public String toString() {
		return "CsvData [lineNum=" + lineNum + ", stationNum=" + stationNum
				+ ", shotTime=" + shotTime + ", gpsTime=" + gpsTime
				+ ", shotId=" + shotId + ", aquisitionLength="
				+ aquisitionLength + ", pointIndex=" + pointIndex
				+ ", upholeTime=" + upholeTime + ", sourceType=" + sourceType
				+ "]";
	}
	
	@Override
	public String toCsv() {
		
		StringBuilder sb = new StringBuilder();
		sb.append(getLineNum()).append(',');
		sb.append(getStationNum()).append(',');
		sb.append(shotTime.format(dateTimeFormat)).append(',');
		sb.append(getGpsTime()).append(',');
		sb.append(getShotId()).append(',');
		sb.append(getAquisitionLength()).append(',');
		sb.append(getPointIndex()).append(',');
		sb.append(getUpholeTime()).append(',');
		sb.append(getSourceType());
		
		return sb.toString();
		
		
	}





	public static class CsvDataBuilder {
		
		private Double lineNum;
		private Double stationNum;
		private LocalDateTime shotTime;
		private Long gpsTime;
		private Integer shotId;
		private Long aquisitionLength;
		private Integer pointIndex = 1;
		private Integer upholeTime = 0;
		private String sourceType = "E1";

		
		public CsvDataBuilder(Double lineNum, Double stationNum) {
			this.lineNum = lineNum;
			this.stationNum = stationNum;
		}
		
		public CsvDataBuilder shotTime(LocalDateTime shotTime) {
			this.shotTime = shotTime;
			return this;
		}
		
		public CsvDataBuilder gpsTime(Long gpsTime) {
			this.gpsTime = gpsTime;
			return this;
		}
		
		public CsvDataBuilder shotId(Integer shotId) {
			this.shotId = shotId;
			return this;
		}
		public CsvDataBuilder aquisitionLength(Long aquisitionLength) {
			this.aquisitionLength = aquisitionLength;
			return this;
		}
		
		public CsvDataBuilder pointIndex(Integer pointIndex) {
			this.pointIndex = pointIndex;
			return this;
		}
		
		public CsvDataBuilder upholeTime(Integer upholeTime) {
			this.upholeTime = upholeTime;
			return this;
		}
		
		public CsvDataBuilder sourceType(String sourcetype) {
			this.sourceType = sourcetype;
			return this;
		}

		public Double getLineNum() {
			return lineNum;
		}

		public Double getStationNum() {
			return stationNum;
		}

		public LocalDateTime getShotTime() {
			return shotTime;
		}

		public Long getGpsTime() {
			return gpsTime;
		}

		public Integer getShotId() {
			return shotId;
		}

		public Long getAquisitionLength() {
			return aquisitionLength;
		}

		public Integer getPointIndex() {
			return pointIndex;
		}

		public Integer getUpholeTime() {
			return upholeTime;
		}

		public String getSourceType() {
			return sourceType;
		}
		
		public CsvData build() {
			return new CsvData(this);
		}
		
	}

}
