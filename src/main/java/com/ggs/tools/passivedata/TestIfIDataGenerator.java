package com.ggs.tools.passivedata;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.ggs.tools.util.DateUtil;

/**
 * Generates CSV Data for Test-If-I input file
 * @author Oliver.Hsu
 *
 */
public class TestIfIDataGenerator {
		
	private TestIfIReader testIfIReader = new TestIfIReader();
	
	private static final int OFFSET_SECS = 30;
	
	
	/**
	 * Generate CSV Data for Test-If-I Data file
	 * @param input
	 * @param testIfIFile
	 * @param timeAdjustment
	 * @return List<CsvData>
	 * @throws IOException
	 */
	public List<CsvData> testIfIDataGenerator (PassiveDataInput input, Path testIfIFile, boolean timeAdjustment) throws IOException {
		
		List<CsvData> csvData = new ArrayList<>();
		List<TestIfIData> testIfIDataList = testIfIReader.readTestIfIFile(testIfIFile);
		
		int shotId = input.getFfid();
		
		for (TestIfIData testIfIData : testIfIDataList) {
			
			LocalDateTime shotStartTime = LocalDateTime.of(testIfIData.getEventDate(), testIfIData.getEventTime().plus(testIfIData.getEventTimeMicroSecs(), ChronoUnit.MICROS));
			if (timeAdjustment) {
				shotStartTime = shotStartTime.minusSeconds(OFFSET_SECS);
			}
			
			// generate csv entry
			CsvData csvEntry = new CsvData.CsvDataBuilder(input.getLineNum(), input.getStationNum()).shotTime(shotStartTime).gpsTime(DateUtil.getGPSTime(shotStartTime.toInstant(ZoneOffset.UTC))).aquisitionLength(input.getTraceLengthMilli()).shotId(shotId).build();
			csvData.add(csvEntry);
			
			// increment the shot id
			shotId = shotId + input.getFfidIncrement();


		}
		
		return csvData;
	}

}
