package com.ggs.tools.passivedata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestIfIReader {
	
	private static  final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private static final Pattern serialNum = Pattern.compile("\\d{3}");
	private static final Logger LOGGER = LoggerFactory.getLogger(TestIfIReader.class);
	
	public List<TestIfIData> readTestIfIFile(Path path) throws IOException {
		
		List<TestIfIData> testIfIDataList = new ArrayList<>();
		
		int lineNum = 1;
		try (Scanner s = new Scanner(Files.newBufferedReader(path))) {
			
			while (s.hasNext()) {
				
				try {
					
					// skip lines that do not start with the 3 digit serial number
					if (!s.hasNext(serialNum)) {
						s.nextLine();
						continue;
					}
					
					
					// read serial num
					s.next();
					
					// read microsecs
					int microSecs = s.nextInt();
					
					// read microsec unit us
					s.next();				

					// Read positive / negative transition
					s.next();				

					// read time U or G for UTC/GPS
					s.next();
					
					// read event time
					String time = s.next();
					LocalTime eventTime = LocalTime.parse(time);
					
					// read number of visible satellites
					s.next();
					
					// read number of satellites used for calculation
					s.next();
					
					// read event date
					String date = s.next();
					LocalDate eventDate = LocalDate.parse(date, dateFormat);
					
					TestIfIData testIfIData = new TestIfIData(microSecs, eventTime, eventDate);

					testIfIDataList.add(testIfIData);
					
					lineNum++;

				} catch (Exception nse) {
					LOGGER.error("parse error at line " + lineNum, nse);
					throw new RuntimeException("Error reading Verif-I file around line " + lineNum, nse);
				
				}
				
			}
			
		}
		
		return testIfIDataList;
		
	}

}
