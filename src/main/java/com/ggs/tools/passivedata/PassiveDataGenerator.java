package com.ggs.tools.passivedata;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.ggs.tools.util.DateUtil;

public class PassiveDataGenerator {

	/**
	 * Given the PassiveDataInput, generates a List<CsvData> of shots
	 * Shots are generated for the days in the start and end date range and for each trace length
	 * between start time and extract duration
	 * 
	 * Shot id and ffid are equivalent
	 * @param input
	 * @return
	 */
	public List<CsvData> generatePassiveData(PassiveDataInput input) {
		
		List<CsvData> csvData = new ArrayList<>();
		
		LocalDate currentDate = input.getStartDate();
		
		int shotId = input.getFfid();
		
		while (currentDate.isBefore(input.getEndDate())) {
		
			LocalDateTime shotStartTime = LocalDateTime.of(currentDate, input.getStartTime());
			LocalDateTime shotEndTime = shotStartTime.plus(Duration.ofMillis(input.getExtractDurationMilli())).minus(Duration.ofMillis(input.getTraceLengthMilli()));
			LocalDateTime currentShotTime = shotStartTime;
			
			while (!currentShotTime.isAfter(shotEndTime)) {
				
				// calculate the shot time				
				ZonedDateTime shotTimeZoned = currentShotTime.atZone(input.getZoneId());
				
				// generate csv entry
				CsvData csvEntry = new CsvData.CsvDataBuilder(input.getLineNum(), input.getStationNum()).shotTime(currentShotTime).gpsTime(DateUtil.getGPSTime(shotTimeZoned.toInstant())).aquisitionLength(input.getTraceLengthMilli()).shotId(shotId).build();
				csvData.add(csvEntry);
				
				// increment the shot time
				currentShotTime = currentShotTime.plus(Duration.ofMillis(input.getTraceLengthMilli()));
				
				// increment the shot id
				shotId = shotId + input.getFfidIncrement();
				
			}
			
			// increment current date
			currentDate = currentDate.plusDays(input.getIntervalDays());
			
			// reset the shot id if needed
			if (input.getResetFfid()) {
				shotId = input.getFfid();

			}
		}
		
		
		return csvData;
		
	}
	
	/**
	 * Calculates start date the way the HDR does it, from the start of the unix epoch and then adding day intervals 
	 * @param startDate Desired start date
	 * @return Start Date as calculated from the epoch.  The returned start date is always equal or after the startDate
	 * parameter
	 */
	public LocalDate alternateScheduleStartDate(LocalDate startDate, int dayInterval)  {
		
		// Get date of epoch
		LocalDate epochDate = null;
		
		// get start date epoch day
		long startEpochDay = startDate.toEpochDay();
		

		if (startEpochDay % dayInterval == 0) {
			// start epoch day is multiple of dayInterval, so set the epoch date = start date
			epochDate = startDate;
		} else {
			// set start epoch date to is next multiple of day interval after the start date
			epochDate = LocalDate.ofEpochDay(0);
			long daysToAdd = ((startEpochDay / dayInterval) + 1) * dayInterval;
			epochDate = epochDate.plusDays(daysToAdd);
		}
		
		return epochDate;
	
		
		
	}
	
	

}
