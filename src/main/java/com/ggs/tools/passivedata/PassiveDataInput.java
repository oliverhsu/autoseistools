package com.ggs.tools.passivedata;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * Input data for PassiveDataGenerator
 * @author Oliver.Hsu
 *
 */
public class PassiveDataInput {
	
	private Double lineNum;
	private Double stationNum;
	private Integer ffid;
	private Integer ffidIncrement;
	private LocalDate startDate;
	private LocalDate endDate;
	private Integer intervalDays;
	private LocalTime startTime;
	private ZoneId zoneId;
	private Long extractDurationMilli;
	private Long traceLengthMilli;
	private Boolean resetFfid;
	
	
	public Double getLineNum() {
		return lineNum;
	}
	public void setLineNum(Double lineNum) {
		this.lineNum = lineNum;
	}
	public Double getStationNum() {
		return stationNum;
	}
	public void setStationNum(Double stationNum) {
		this.stationNum = stationNum;
	}
	public Integer getFfid() {
		return ffid;
	}
	public void setFfid(Integer ffid) {
		this.ffid = ffid;
	}
	public Integer getFfidIncrement() {
		return ffidIncrement;
	}
	public void setFfidIncrement(Integer ffidIncrement) {
		this.ffidIncrement = ffidIncrement;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public Integer getIntervalDays() {
		return intervalDays;
	}
	public void setIntervalDays(Integer intervalDays) {
		this.intervalDays = intervalDays;
	}
	public LocalTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}
	public ZoneId getZoneId() {
		return zoneId;
	}
	public void setZoneId(ZoneId zoneId) {
		this.zoneId = zoneId;
	}
	public Long getExtractDurationMilli() {
		return extractDurationMilli;
	}
	public void setExtractDurationMilli(Long extractDurationMilli) {
		this.extractDurationMilli = extractDurationMilli;
	}
	public Long getTraceLengthMilli() {
		return traceLengthMilli;
	}
	public void setTraceLengthMilli(Long traceLengthMilli) {
		this.traceLengthMilli = traceLengthMilli;
	}
	public void setResetFfid(Boolean resetFfid) {
		this.resetFfid = resetFfid;
	}
	public Boolean getResetFfid() {
		return resetFfid;
	}	


}
