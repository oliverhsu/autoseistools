package com.ggs.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

/**
 * Main class for starting AutoSeis Tools
 * @author Oliver.Hsu
 *
 */
public class ToolKit extends Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToolKit.class);
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
    public void start(Stage stage) throws Exception {
		LOGGER.info("Tool Kit selection screen is opening");
       Parent root = FXMLLoader.load(getClass().getResource("/fxml/ToolKit.fxml"));    
        Scene scene = new Scene(root);    
        stage.setTitle("AutoSeis ToolKit");
        stage.setScene(scene);
        stage.setFullScreen(false);
        stage.show();
    }
	
	@Override
	public void stop() throws Exception {
		super.stop();		
		LOGGER.info("Stopping AutoSeis Tools");
		System.exit(0);

	}
	
}
