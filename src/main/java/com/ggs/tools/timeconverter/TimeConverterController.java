/**
 * 
 */
package com.ggs.tools.timeconverter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import jfxtras.scene.control.LocalDateTimePicker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ggs.tools.util.DateUtil;
import com.ggs.tools.util.FileDialogs;

/**
 * @author Febin.kuriakose
 *
 */
public class TimeConverterController implements Initializable {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TimeConverterController.class);
	
	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#");
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/YYYY HH:mm:ss:SSSS");
	
	private static final DateTimeFormatter PARSE_DATE_FORMAT = DateTimeFormatter.ofPattern("MM/dd/YYYY HH:mm:ss:SSSS");
	
	private final LocalDateTime localDateTime = LocalDateTime.now();
	
	private final ZoneOffset utcOffset = ZoneOffset.of("Z");
	
	@FXML
	private HBox intimeHBox;
	
	@FXML
	private HBox fileInHBox;
	
	@FXML
	private HBox dateInHBox;
	
	@FXML
	private HBox timeZoneHBox;
	
	@FXML
	private TextField timeInTextField;
	
	@FXML
	private Label fileNameLabel;
	
	@FXML
	private TableView<TimeData> timeTableView;
	
	@FXML
	private TableColumn<TimeData, Long> gpsTimeCol;
	
	@FXML
	private TableColumn<TimeData, Long> epochTimeCol;
	
	@FXML
	private TableColumn<TimeData, String> dateCol;
	
	@FXML
	private TableColumn<TimeData, String> testifICol;
	
	@FXML
	private RadioButton singleInRButton;

	@FXML
	private RadioButton fileInRButton;
	
	@FXML
	private RadioButton gpsSrcRButton;
	
	@FXML
	private RadioButton epochsSrcRButton;
	
	@FXML
	private RadioButton dateSrcRButton;
	
	@FXML
	private RadioButton testifISrcRButton;
	
	@FXML
	private AnchorPane timeConverterAnchorPane;
	
	@FXML
	private ComboBox<ZoneId> timeZoneCombo;
	
	@FXML
	private ComboBox<ZoneId> srcTimeZoneCombo;
	
	private LocalDateTimePicker dateTimePicker;
	
	private File inputFile;
	
	private ObservableList<TimeData> timeDatas = FXCollections.observableArrayList();
	
	/* 
	 * Method to initialize the screen
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		LOGGER.info("Initializing Time Converter Screen");
		dateTimePicker = new LocalDateTimePicker();
		dateInHBox.getChildren().add(0, new Label("Date"));
		dateInHBox.getChildren().add(1,dateTimePicker);
		
		//Initialize Table
		gpsTimeCol.setCellValueFactory(new PropertyValueFactory<TimeData, Long>("gpsTime"));
		epochTimeCol.setCellValueFactory(new PropertyValueFactory<TimeData, Long>("epochTime"));
		dateCol.setCellValueFactory(new PropertyValueFactory<TimeData, String>("date"));
		testifICol.setCellValueFactory(new PropertyValueFactory<TimeData, String>("testifI"));
		gpsTimeCol.setCellFactory(new Callback<TableColumn<TimeData, Long>, TableCell<TimeData, Long>>() {
			@Override
			public TableCell<TimeData, Long> call(TableColumn<TimeData, Long> param) {
				return new TableCell<TimeData, Long>() {
					@Override
					protected void updateItem(Long item, boolean empty) {
						super.updateItem(item, empty);		            	
						if (!empty && item!=null) {		                
							setText(DECIMAL_FORMAT.format(item));
						} else {
							setText(null);
						}
					}
				};
			}
		});
		epochTimeCol.setCellFactory(new Callback<TableColumn<TimeData, Long>, TableCell<TimeData, Long>>() {
			@Override
			public TableCell<TimeData, Long> call(TableColumn<TimeData, Long> param) {
				return new TableCell<TimeData, Long>() {
					@Override
					protected void updateItem(Long item, boolean empty) {
						super.updateItem(item, empty);		            	
						if (!empty && item!=null) {		                
							setText(DECIMAL_FORMAT.format(item));
						} else {
							setText(null);
						}
					}
				};
			}
		});		
		timeTableView.setItems(timeDatas);
		//Initialize combo box		
		List<String> zoneList = new ArrayList<String>(ZoneId.getAvailableZoneIds());
		Collections.sort(zoneList);		
		for (String s : zoneList) {		   
		    timeZoneCombo.getItems().add(ZoneId.of(s));
		    srcTimeZoneCombo.getItems().add(ZoneId.of(s));
		}		
		timeZoneCombo.setValue(ZoneId.systemDefault());
		// set custom renderer for combobox to show time zone with utc offset
		timeZoneCombo.setCellFactory(new Callback<ListView<ZoneId>, ListCell<ZoneId>>() {
            @Override
            public ListCell<ZoneId> call(ListView<ZoneId> arg0) {
                return new ListCell<ZoneId>() {
                    @Override
                    protected void updateItem(ZoneId zone, boolean empty) {
                        super.updateItem(zone, empty);
                        if (zone != null || !empty) {
                		    ZonedDateTime zdt = localDateTime.atZone(zone);
                		    ZoneOffset offset = zdt.getOffset();
                		    String zoneString = String.format("%s %s\n", zone, offset.equals(utcOffset) ? "UTC" : offset);
                            setText(zoneString);                            
                        }
                    }
                };
            }
        });
		srcTimeZoneCombo.setValue(ZoneId.systemDefault());
		// set custom renderer for combobox to show time zone with utc offset
		srcTimeZoneCombo.setCellFactory(new Callback<ListView<ZoneId>, ListCell<ZoneId>>() {
            @Override
            public ListCell<ZoneId> call(ListView<ZoneId> arg0) {
                return new ListCell<ZoneId>() {
                    @Override
                    protected void updateItem(ZoneId zone, boolean empty) {
                        super.updateItem(zone, empty);
                        if (zone != null || !empty) {
                		    ZonedDateTime zdt = localDateTime.atZone(zone);
                		    ZoneOffset offset = zdt.getOffset();
                		    String zoneString = String.format("%s %s\n", zone, offset.equals(utcOffset) ? "UTC" : offset);
                            setText(zoneString);                            
                        }
                    }
                };
            }
        });
		//Show hide default controls
		handleSelectInputType();
	}
	
	private Stage getParentStage() {
		return (Stage) timeConverterAnchorPane.getScene().getWindow();
	}

	
	/**
	 * Method to convert input data
	 */
	public void handleConvertButton(){
		timeDatas.clear();
		//Invoke method based on source type selection
		if(gpsSrcRButton.isSelected()){
			convertLongTime(1);
		}
		if(epochsSrcRButton.isSelected()){
			convertLongTime(2);
		}
		if(dateSrcRButton.isSelected()){
			convertDate();
		}
		if(testifISrcRButton.isSelected()){
			convertTestifI();
		}
	}
	
	/**
	 * Converts the input GPS time to all formats
	 */
	private void convertLongTime(int longTimeType){
		List<Long> list = new ArrayList<Long>();
		if(singleInRButton.isSelected()){
			//Reading single value
			list.add(Long.parseLong(timeInTextField.getText()));
		}else{
			//Reading from file
			list.addAll(readLongValues());
		}
		List<TimeData> timeDataList = new ArrayList<TimeData>();
		ZoneId zoneId = timeZoneCombo.getSelectionModel().getSelectedItem();
		for(Long longTime : list){
			//Creating TimeData object for each time
			TimeData timeData = new TimeData();
			Date date = null;
			if(longTimeType==1){
				timeData.setGpsTime(longTime);
				date = DateUtil.getDateFromGPSTime(longTime);
				timeData.setEpochTime(DateUtil.getEpochTime(date));
			}else{
				timeData.setEpochTime(longTime);
				date = DateUtil.getDateFromEpochTime(longTime);				
				timeData.setGpsTime(DateUtil.getGPSTime(date));
			}	
			DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(zoneId));
			timeData.setDate(DATE_FORMAT.format(date));					
			timeData.setTestifI(DateUtil.getTestifIFromDate(date));
			timeDataList.add(timeData);
		}
		if(!timeDataList.isEmpty()){
			timeDatas.addAll(timeDataList);
		}
	}
	
	/**
	 * Reading GPS time and Epoch Time
	 * @return
	 */
	private List<Long> readLongValues(){
		List<Long> longs = new ArrayList<Long>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(inputFile));		 
			String line = null;
			while ((line = br.readLine()) != null) {
				longs.add(Long.parseLong(line));
			}	 
			br.close();
		} catch (Exception e) {
			LOGGER.error("Error occurred while reading file",e);
		} 
		return longs;
	}
	
	/**
	 * Reading date values
	 * @return
	 */
	private List<Date> readDateValues(ZoneId zoneId){
		List<Date> dates = new ArrayList<Date>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(inputFile));		 
			String line = null;
			while ((line = br.readLine()) != null) {
				try {
					LocalDateTime dateTime = LocalDateTime.parse(line, PARSE_DATE_FORMAT);
					dates.add(DateUtil.getDate(dateTime, zoneId));
				} catch (Exception e) {
					LOGGER.debug("Not a valid date format (MM/dd/YYYY HH:mm:ss:SSSS) {} ",line);
				}
			}	 
			br.close();
		} catch (Exception e) {	
			LOGGER.error("Error occurred while reading file",e);
		}			
		return dates;
	}
	
	/**
	 * Reading TESTIF-I values
	 * @return
	 */
	private List<String> readStringValues(){
		List<String> strings = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(inputFile));		 
			String line = null;
			while ((line = br.readLine()) != null) {
				strings.add(line);
			}	 
			br.close();
		} catch (Exception e) {	
			LOGGER.error("Error occurred while reading file",e);
		}			
		return strings;
	}

	/**
	 * Convert Date type to other formats
	 */
	private void convertDate(){
		List<Date> list = new ArrayList<Date>();
		ZoneId zoneId = srcTimeZoneCombo.getSelectionModel().getSelectedItem();
		if(singleInRButton.isSelected()){
			//Reading single value
			list.add(DateUtil.getDate(dateTimePicker.getLocalDateTime(), zoneId));
		}else{
			list.addAll(readDateValues(zoneId));
		}
		List<TimeData> timeDataList = new ArrayList<TimeData>();
		ZoneId tZoneId = timeZoneCombo.getSelectionModel().getSelectedItem();
		for(Date date : list){
			TimeData timeData = new TimeData();	
			DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(tZoneId));
			timeData.setDate(DATE_FORMAT.format(date));			
			timeData.setEpochTime(DateUtil.getEpochTime(date));		
			timeData.setGpsTime(DateUtil.getGPSTime(date));
			timeData.setTestifI(DateUtil.getTestifIFromDate(date));
			timeDataList.add(timeData);
		}
		if(!timeDataList.isEmpty()){
			timeDatas.addAll(timeDataList);
		}
	}
	
	/**
	 * Convert TESTIF-I type to other formats
	 */
	private void convertTestifI(){
		List<String> list = new ArrayList<String>();
		if(singleInRButton.isSelected()){
			//Reading single value
			list.add(timeInTextField.getText());
		}else{
			list.addAll(readStringValues());
		}
		List<TimeData> timeDataList = new ArrayList<TimeData>();
		ZoneId zoneId = timeZoneCombo.getSelectionModel().getSelectedItem();
		for(String testifI : list){
			TimeData timeData = new TimeData();	
			timeData.setTestifI(testifI);
			Date date=null;
			try {
				date = DateUtil.getDateFromTestifIFormat(testifI);
			} catch (ParseException e) {
				LOGGER.error("Error occurred while converting testif-i to date",e);
			}
			DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(zoneId));
			timeData.setDate(DATE_FORMAT.format(date));		
			timeData.setEpochTime(DateUtil.getEpochTime(date));		
			timeData.setGpsTime(DateUtil.getGPSTime(date));			
			timeDataList.add(timeData);
		}
		if(!timeDataList.isEmpty()){
			timeDatas.addAll(timeDataList);
		}
	}
	
	/**
	 * Method to show file select dialog
	 */
	public void handleBrowseButton(){
		//Open the dialog to select file
		inputFile = FileDialogs.showFileChooser("Select Input File", null, getParentStage());
		if(inputFile!=null && inputFile.exists()){
			fileNameLabel.setText(inputFile.getAbsolutePath());
		}		
	}
	
	/**
	 * Method to show/hide input types
	 */
	public void handleSelectInputType(){
		//Show single input controls
		if(singleInRButton.isSelected()){
			//Show date time picker if date source is selected
			if(dateSrcRButton.isSelected()){
				dateInHBox.managedProperty().bind(dateInHBox.visibleProperty());
				dateInHBox.setVisible(true);
				timeZoneHBox.managedProperty().bind(timeZoneHBox.visibleProperty());
				timeZoneHBox.setVisible(true);
				intimeHBox.managedProperty().bind(intimeHBox.visibleProperty());
				intimeHBox.setVisible(false);
			}else{
				dateInHBox.managedProperty().bind(dateInHBox.visibleProperty());
				dateInHBox.setVisible(false);
				timeZoneHBox.managedProperty().bind(timeZoneHBox.visibleProperty());
				timeZoneHBox.setVisible(false);
				intimeHBox.managedProperty().bind(intimeHBox.visibleProperty());
				intimeHBox.setVisible(true);
			}			
			fileInHBox.managedProperty().bind(fileInHBox.visibleProperty());
			fileInHBox.setVisible(false);	
		}
		if(fileInRButton.isSelected()){
			dateInHBox.managedProperty().bind(dateInHBox.visibleProperty());
			dateInHBox.setVisible(false);
			if(dateSrcRButton.isSelected()){
				timeZoneHBox.managedProperty().bind(timeZoneHBox.visibleProperty());
				timeZoneHBox.setVisible(true);
			}else{
				timeZoneHBox.managedProperty().bind(timeZoneHBox.visibleProperty());
				timeZoneHBox.setVisible(false);
			}			
			intimeHBox.managedProperty().bind(intimeHBox.visibleProperty());
			intimeHBox.setVisible(false);
			fileInHBox.managedProperty().bind(fileInHBox.visibleProperty());
			fileInHBox.setVisible(true);	
		}			
	}
	
	/**
	 * Method to save the output of conversion to file
	 */
	public void handleSaveToFile(){	
		LOGGER.debug("Saving the time converter out put to File");
		File outputLoc =  FileDialogs.showDirChooser("Select Folder to Save Output", null, getParentStage());
		if(outputLoc!=null){
			try {
				File out = new File(outputLoc.getPath() + File.separator + "Output.txt");
				FileWriter fileWriter = new FileWriter(out.getAbsoluteFile());
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				//Write timdata object to file
				for(TimeData timeData : timeDatas){
					String line = DECIMAL_FORMAT.format(timeData.getGpsTime()) + "," +  DECIMAL_FORMAT.format(timeData.getEpochTime())
							+ "," + DATE_FORMAT.format(timeData.getDate()) + "," + timeData.getTestifI();
					bufferedWriter.write(line);
				}
				bufferedWriter.close();
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}
	}
}
