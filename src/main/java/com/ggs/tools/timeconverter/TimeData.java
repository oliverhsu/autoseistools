/**
 * 
 */
package com.ggs.tools.timeconverter;


/**
 * @author Febin.kuriakose
 *
 */
public class TimeData {
	
	private Long gpsTime;
	private Long epochTime;
	private String date;
	private String testifI;	
	
	public TimeData() {
		super();
	}
	
	public TimeData(Long gpsTime, Long epochTime, String date, String testifI) {
		super();
		this.gpsTime = gpsTime;
		this.epochTime = epochTime;
		this.date = date;
		this.testifI = testifI;
	}

	public Long getGpsTime() {
		return gpsTime;
	}

	public void setGpsTime(Long gpsTime) {
		this.gpsTime = gpsTime;
	}

	public Long getEpochTime() {
		return epochTime;
	}

	public void setEpochTime(Long epochTime) {
		this.epochTime = epochTime;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTestifI() {
		return testifI;
	}

	public void setTestifI(String testifI) {
		this.testifI = testifI;
	}
}
