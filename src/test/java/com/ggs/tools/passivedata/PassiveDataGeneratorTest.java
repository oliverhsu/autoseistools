package com.ggs.tools.passivedata;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PassiveDataGeneratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testResetFfidTrue() {
		
		LocalDate currentDate = LocalDate.now();
		
		PassiveDataInput passiveDataInput = new PassiveDataInput();
		passiveDataInput.setStartDate(currentDate);
		passiveDataInput.setEndDate(currentDate.plusDays(7));
		// extract duration of 5 minutes
		passiveDataInput.setExtractDurationMilli(6*60*1000l);
		passiveDataInput.setFfid(1);
		passiveDataInput.setFfidIncrement(1);
		passiveDataInput.setIntervalDays(2);
		passiveDataInput.setLineNum(5001d);
		passiveDataInput.setStationNum(1001d);
		passiveDataInput.setResetFfid(true);
		passiveDataInput.setStartTime(LocalTime.of(4, 0));
		// trace length of 1 minute
		passiveDataInput.setTraceLengthMilli(1*60*1000l);
		passiveDataInput.setZoneId(ZoneId.systemDefault());
		
		PassiveDataGenerator passiveDataGenerator = new PassiveDataGenerator();
		List<CsvData> csvData = passiveDataGenerator.generatePassiveData(passiveDataInput);
		
		// check proper number of records
		assertEquals(24, csvData.size());
		
		// verify ffid resets
		for (CsvData csvEntry : csvData) {
			assertTrue(csvEntry.getShotId() < 7);
		}
		

	}

	@Test
	public void testResetFfidFalse() {
		
		LocalDate currentDate = LocalDate.now();
		
		PassiveDataInput passiveDataInput = new PassiveDataInput();
		passiveDataInput.setStartDate(currentDate);
		passiveDataInput.setEndDate(currentDate.plusDays(7));
		// extract duration of 5 minutes
		passiveDataInput.setExtractDurationMilli(6*60*1000l);
		passiveDataInput.setFfid(1);
		passiveDataInput.setFfidIncrement(1);
		passiveDataInput.setIntervalDays(2);
		passiveDataInput.setLineNum(5001d);
		passiveDataInput.setStationNum(1001d);
		passiveDataInput.setResetFfid(false);
		passiveDataInput.setStartTime(LocalTime.of(4, 0));
		// trace length of 1 minute
		passiveDataInput.setTraceLengthMilli(1*60*1000l);
		passiveDataInput.setZoneId(ZoneId.systemDefault());
		
		PassiveDataGenerator passiveDataGenerator = new PassiveDataGenerator();
		List<CsvData> csvData = passiveDataGenerator.generatePassiveData(passiveDataInput);
		
		// check proper number of records
		assertEquals(24, csvData.size());
		
		// verify ffid resets
		for (int i = 0; i < csvData.size(); i++) {
			assertEquals(i+1, csvData.get(i).getShotId().intValue());
		}
		

	}
	
	@Test
	public void testFfidIncrement() {
		
		LocalDate currentDate = LocalDate.now();
		
		PassiveDataInput passiveDataInput = new PassiveDataInput();
		passiveDataInput.setStartDate(currentDate);
		passiveDataInput.setEndDate(currentDate.plusDays(7));
		// extract duration of 5 minutes
		passiveDataInput.setExtractDurationMilli(6*60*1000l);
		passiveDataInput.setFfid(1);
		passiveDataInput.setFfidIncrement(2);
		passiveDataInput.setIntervalDays(2);
		passiveDataInput.setLineNum(5001d);
		passiveDataInput.setStationNum(1001d);
		passiveDataInput.setResetFfid(false);
		passiveDataInput.setStartTime(LocalTime.of(4, 0));
		// trace length of 1 minute
		passiveDataInput.setTraceLengthMilli(1*60*1000l);
		passiveDataInput.setZoneId(ZoneId.systemDefault());
		
		PassiveDataGenerator passiveDataGenerator = new PassiveDataGenerator();
		List<CsvData> csvData = passiveDataGenerator.generatePassiveData(passiveDataInput);
		
		// check proper number of records
		assertEquals(24, csvData.size());
		
		// verify ffid (aka shotid) incremetns correctly
		int shotId = 1;
		for (int i = 0; i < csvData.size(); i++) {
			assertEquals(shotId, csvData.get(i).getShotId().intValue());
			shotId = shotId + 2;
		}
		

	}
	
	
	@Test
	public void testIntervalDays() {
		
		LocalDate currentDate = LocalDate.now();
		
		PassiveDataInput passiveDataInput = new PassiveDataInput();
		passiveDataInput.setStartDate(currentDate);
		passiveDataInput.setEndDate(currentDate.plusDays(7));
		// extract duration of 5 minutes
		passiveDataInput.setExtractDurationMilli(6*60*1000l);
		passiveDataInput.setFfid(1);
		passiveDataInput.setFfidIncrement(1);
		passiveDataInput.setIntervalDays(1);
		passiveDataInput.setLineNum(5001d);
		passiveDataInput.setStationNum(1001d);
		passiveDataInput.setResetFfid(true);
		passiveDataInput.setStartTime(LocalTime.of(4, 0));
		// trace length of 1 minute
		passiveDataInput.setTraceLengthMilli(1*60*1000l);
		passiveDataInput.setZoneId(ZoneId.systemDefault());
		
		PassiveDataGenerator passiveDataGenerator = new PassiveDataGenerator();
		List<CsvData> csvData = passiveDataGenerator.generatePassiveData(passiveDataInput);
		
		// check proper number of records
		assertEquals(42, csvData.size());
		

	}
	

}
