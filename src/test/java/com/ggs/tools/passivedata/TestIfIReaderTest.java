package com.ggs.tools.passivedata;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestIfIReaderTest {

	TestIfIReader testIfIReader;
	
	@Before
	public void setUp() throws Exception {
		
		testIfIReader = new TestIfIReader();
	}

	@Test
	public void testReader() throws IOException {
		
		List<TestIfIData> testIfIData = testIfIReader.readTestIfIFile(Paths.get("src/test/resources/testifi_example.txt"));
		
		assertEquals(6, testIfIData.size());
		
		// check first record
		TestIfIData data = testIfIData.get(0);
		assertEquals(804014, data.getEventTimeMicroSecs());
		assertEquals(LocalTime.of(16,24,02), data.getEventTime());
		assertEquals(LocalDate.of(2013, 10, 02), data.getEventDate());
		
	}

}
