package com.ggs.tools.util;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;

public class DateUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetGpsTime() {
		
		ZonedDateTime dt = ZonedDateTime.of(2014, 12, 8, 8, 50, 00, 0, ZoneId.of("America/Chicago"));
		long timeGpsMicroSec = 1102085416000000l;
		
		long convertedGps = DateUtil.getGPSTime(dt.toInstant());
		
		assertEquals(timeGpsMicroSec, convertedGps);
		
		
	}
	
	@Test
	public void testGetGpsTimeWithNano() {
		
		ZonedDateTime dt = ZonedDateTime.of(2014, 12, 8, 8, 50, 00, 963427000, ZoneId.of("America/Chicago"));
		long timeGpsMicroSec = 1102085416963427l;
		
		long convertedGps = DateUtil.getGPSTime(dt.toInstant());
		
		assertEquals(timeGpsMicroSec, convertedGps);
		
		
	}
	
	@Test
	public void testGPSTime(){
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("America/Chicago")));
		calendar.set(2014, 11, 9, 10, 35, 15);
		Long gpsTime = DateUtil.getGPSTime(calendar.getTime());
		assertEquals(1102178131*1000000L, gpsTime.longValue());
		//Test reverse conversion
		Date date = DateUtil.getDateFromGPSTime(gpsTime);
		Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("America/Chicago")));
		calendar2.setTime(date);
		assertEquals(calendar.get(Calendar.YEAR), calendar2.get(Calendar.YEAR));
		assertEquals(calendar.get(Calendar.MONTH), calendar2.get(Calendar.MONTH));
		assertEquals(calendar.get(Calendar.HOUR), calendar2.get(Calendar.HOUR));
		assertEquals(calendar.get(Calendar.MINUTE), calendar2.get(Calendar.MINUTE));
		assertEquals(calendar.get(Calendar.SECOND), calendar2.get(Calendar.SECOND));
	}
	
	
	@Test
	public void testEpochTime(){
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("America/Chicago")));
		calendar.set(2014, 11, 9, 10, 35, 15);
		Long epochTime = DateUtil.getEpochTime(calendar.getTime());
		assertEquals(1418142915, epochTime.longValue());
		//Test reverse conversion
		Date date = DateUtil.getDateFromEpochTime(epochTime);
		Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("America/Chicago")));
		calendar2.setTime(date);
		assertEquals(calendar.get(Calendar.YEAR), calendar2.get(Calendar.YEAR));
		assertEquals(calendar.get(Calendar.MONTH), calendar2.get(Calendar.MONTH));
		assertEquals(calendar.get(Calendar.HOUR), calendar2.get(Calendar.HOUR));
		assertEquals(calendar.get(Calendar.MINUTE), calendar2.get(Calendar.MINUTE));
		assertEquals(calendar.get(Calendar.SECOND), calendar2.get(Calendar.SECOND));
	}
	
	@Test
	public void testTestIfITime() throws ParseException{
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("America/Chicago")));
		calendar.set(2014, 11, 9, 10, 35, 15);
		String testifI = DateUtil.getTestifIFromDate(calendar.getTime());
		assertEquals("001   000000 us U + 10:35:15 01 01 09/12/2014".length(),testifI.length());
		//Test reverse conversion
		Date date = DateUtil.getDateFromTestifIFormat(testifI);
		Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("America/Chicago")));
		calendar2.setTime(date);
		assertEquals(calendar.get(Calendar.YEAR), calendar2.get(Calendar.YEAR));
		assertEquals(calendar.get(Calendar.MONTH), calendar2.get(Calendar.MONTH));
		assertEquals(calendar.get(Calendar.HOUR), calendar2.get(Calendar.HOUR));
		assertEquals(calendar.get(Calendar.MINUTE), calendar2.get(Calendar.MINUTE));
		assertEquals(calendar.get(Calendar.SECOND), calendar2.get(Calendar.SECOND));
	}
	
	@Test
	public void testTestIfIFormat() throws ParseException{
		Date date = DateUtil.getDateFromTestifIFormat("");
		assertNull(date);
		date = DateUtil.getDateFromTestifIFormat("195789 us + U 17:51:47 13 08 09/02/2011");
		assertNull(date);
		date = DateUtil.getDateFromTestifIFormat("058  195789 us + U 17:51:47 13 0809/02/2011");
		assertNull(date);
		date = DateUtil.getDateFromTestifIFormat("058 195789 us +       U 17:51:47       13 08         09/02/2011");
		assertNotNull(date);
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("America/Chicago")));
		calendar.setTime(date);
		assertEquals(calendar.get(Calendar.YEAR), 2011);
		date = DateUtil.getDateFromTestifIFormat("058   21275 us + U 17:53:27 13 10 09/02/2011");
		assertNotNull(date);		
	}
}
